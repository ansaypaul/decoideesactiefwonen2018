<?php get_header(); ?>
			<main class="main">
			<div class="content full-content"></div> 
				<!-- Part 0 -->				
				<div id="container" class="container-main no_desktop">
					<?php global $excluded_ID; ?>
					<!-- changer actu/news -->
					<?php $query = new WP_Query( array ( 'posts_per_page' => 4, 'post__in' => $excluded_ID, 'order' => 'DESC')); ?>
					<?php  if ( $query->post_count >= 1)  { $i = 0; ?>
						<?php while ( $query->have_posts() ) : $query->the_post(); $i++?>
							<?php if($i == 1){ ?>
								<div class="row">
									<?php get_template_part( 'templates/template_post' ); ?>
								</div> 
							<?php } elseif (($i == 2)) { ?>
									<div class="row row-imu">
										<div id="imu-<?php display_id_random()?>" class="ads " data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
									</div>
								<div class="row" class=""> 
									<?php get_template_part( 'templates/template_post' ); ?>
								</div> 
							<?php } elseif ($i == 3) { ?>
								<div class="row" >
									<?php get_template_part( 'templates/template_post' ); ?>
								</div>
							<?php } elseif ($i == 4) { ?>
								<div class="row">
									<?php get_template_part( 'templates/template_post' ); ?>
								</div>
							<?php } ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					<?php } ?>
				</div>
				<!-- Part 1 -->

				<div class="container-main">
					<!-- changer actu/news -->
					<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ 
						$query = new WP_Query( array ( 'posts_per_page' => 3, 'post__not_in' => $excluded_ID, 'category_name' => 'news', 'order' => 'DESC'));
					 } else {  
					 	$query = new WP_Query( array ( 'posts_per_page' => 3, 'post__not_in' => $excluded_ID, 'category_name' => 'nieuws', 'order' => 'DESC'));
					 }; ?>
					<?php  if ( $query->post_count >= 1)  { $i = 0; ?>
						<?php while ( $query->have_posts() ) : $query->the_post(); $i++?>
							<?php $excluded_ID[] = get_the_ID(); ?>
							<?php if($i == 1){ ?>
								<div class="row row-news">
									<div class="news">
										<div class="_color"><?php _e( "News", "html5blank" ); ?></div>
									</div>
								</div>
								<div class="row">
									<?php get_template_part( 'templates/template_post' ); ?>
								</div>
							<?php } elseif (($i == 2)) { ?>
								<div class="row row-imu">
									<div id="imu-<?php display_id_random()?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
									<!-- <?php $rand = rand(1,6) ?>
									<a target="_blank" href="https://www.sothebysrealty.be/"><img src="<?php bloginfo("template_directory"); ?>/pubs/sothebys/300x250BAN<?php echo $rand; ?>.jpg"></a> -->
									<!-- Bois et habitat >> 26/03  -->
									<!-- <a target="_blank" href="https://www.bois-habitat.be/fr/Tickets-D-I"><img src="<?php bloginfo("template_directory"); ?>/img/pubs/banner_boisethabitat.jpg"></a> -->
								</div>

								<div class="row no-pad">
									<?php get_template_part( 'templates/template_post' ); ?>
								</div>
							<?php } elseif ($i == 3) { ?>
								<div class="row">
									<?php get_template_part( 'templates/template_post' ); ?>
								</div>
								<div class="row row-fav">
									<?php get_template_part( 'templates/template_mostpopular' ); ?>
								</div>
							<?php } ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					<?php } ?>
				</div>

				<div id="billboard-<?php display_id_random()?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position=""></div>

				<div class="container-main">
					<!-- //online Non classé ID=1 // Concours ID=1881 //Publi ID=8 -->
					<?php $query = new WP_Query( array ( 'posts_per_page' => 5, 'category__not_in' => array( 1, 1881, 2051 ), 'post__not_in' => $excluded_ID, 'order' => 'DESC')); ?>
						<?php  if ( $query->post_count >= 1)  { ?>
							<?php while ( $query->have_posts() ) : $query->the_post(); $u++?>
								<?php $excluded_ID[] = get_the_ID(); ?>
								<?php if(($u == 1)){ ?>
									<div class="container-main container-full">
										<div class="row-full">
											<?php get_template_part( 'templates/template_fullpost' ); ?>
										</div>
									</div>
								<?php } elseif ($u == 3) { ?>
									<div class="row">
										<?php get_template_part( 'templates/template_post' ); ?>
									</div>
									<div class="row row-imu">
										<div id="imu-<?php display_id_random()?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
										<!-- Journée de l'Artisan >> 18/11  -->
										<!-- <a target="_blank" href="https://www.journeedelartisan.be/fr/"><img src="<?php bloginfo("template_directory"); ?>/pubs/Banner-Deco-300x250-FR-v2-sm.gif"></a> -->
									</div>
								<?php } elseif ($u == 5) { ?> 
									<div class="row">
										<?php get_template_part( 'templates/template_post' ); ?>
									</div>
									<div class="row row-fav">
										<?php get_template_part( 'templates/template_contest' ); ?>
									</div>
								<?php } else { ?>
									<div class="row">
										<?php get_template_part( 'templates/template_post' ); ?>
									</div>
								<?php } ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					<?php } ?> 
				</div>

				<div id="billboard-<?php display_id_random()?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="" data-refadMOB="" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position=""></div>

				<div class="container-main">
					<?php $query = new WP_Query( array ( 'posts_per_page' => 7, 'category__not_in' => array( 1, 1881, 2051 ), 'post__not_in' => $excluded_ID, 'order' => 'DESC')); ?>
						<?php  if ( $query->post_count >= 1)  { ?>
							<?php while ( $query->have_posts() ) : $query->the_post(); $t++?>
								<?php $excluded_ID[] = get_the_ID(); ?>
								<?php if(($t == 1)){ ?>
									<?php $excluded_ID[] = get_the_ID(); ?>
									<div class="container-main container-full">
										<div class="row-full">
											<?php get_template_part( 'templates/template_fullpost' ); ?>
										</div>
									</div>
								<?php } elseif ($t == 4) { ?>
									<div class="row row-imu">
										<div id="imu-<?php display_id_random()?>" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="Middle" data-refad="pebbleMiddle" data-location="homepage" data-position="2"></div>
									</div>
									<!-- IMU STOKE 07/11 au 20/11  -
									  <div class="row row-imu">
									  <div class="imu ad ad--imu" format="" position="1">
											<a target="_blank" href="http://www.easyfairsevents.com/BOI17/normal/BOI17.php?lang=fr#/home&alt_track_id=MP_20170306_discountticket_imu_websiteDI_FR"><img src="<?php bloginfo("template_directory"); ?>/assets/pubs/boisethabitat.jpg"></a>
										</div> 
									</div>-->
								<?php } else { ?>
									<div class="row">
										<?php get_template_part( 'templates/template_post' ); ?>
									</div>
								<?php } ?>
							<?php wp_reset_postdata(); ?>
						<?php endwhile; ?>
					<?php } ?> 
				</div>
			</main>
<?php get_footer(); ?>