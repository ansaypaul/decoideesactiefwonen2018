var $winHeight = $(window).innerHeight();
var $winWidth = $(window).innerWidth();

var $footerHeight;
var $docHeightWithoutFooter;

var $sidebarHeight;
var $sidebarWidth;

var $sidebarOffsetTop;
var $sidebarOffsetLeft;

var $sidebarOffsetBottom;
var $sidebarOffsetRight;

var $docHeight = $(document).height();
var $docWidth = $(document).innerWidth();

var $scrollTop = $(window).scrollTop();
var $device;

var path_windows = window.location.href;
var arr = path_windows.split("/");
var path_web = arr[0]+'/'+arr[1]+'/'+arr[2]+'/'+arr[3];
/* <![CDATA[ */
if(lang_identifiant == 'FR'){
	var ajax_search = {"ajaxurl": "https://www.decoidees.be/wp-admin/admin-ajax.php"};
}else{
	var ajax_search = {"ajaxurl": "https://www.actiefwonen.be/wp-admin/admin-ajax.php"};
}
console.log(ajax_search);
/* ]]> */


function checkWindowCommun() {
	if ($winWidth <= 768) {
		$device = 'sm';
		$('.responsive-ul').css('height', ($winHeight-40));
		goBurger()
	} else if ($winWidth <= 1200)  {
		// $('.header-blank').css('height', ($winHeight-50));
		$device = 'md';
	} else {
		$device = 'lg';
		// $('.header-blank').css('height', ($winHeight-60));
	}
}

function goBurger() {
	$('.burger').click(function() {
		$(this).toggleClass('active');
		$(".responsive-ul").toggleClass('active');
	});
}

function goSearch() {
	$('.search, .search-closebtn').click(function() {
		$(".search-wrapper").toggleClass('active');
		$(".search-input").focus();
		$(".responsive-ul, .burger").removeClass('active');
	});
}

function goNews() {
	$('.news-popup, .modal-closebtn').click(function() {
		$(".full-modal-news").toggleClass('active');
		$(".full-modal-news-input" ).focus();
	});
}

function goCloseLeaderboard() {
	$('.ad-closebtn').click(function() {
		$(".leaderboard").toggleClass('active');
		$('.leaderboard').addClass('no_display');
	});
}

function backtotop() {
	$('.backtotop').click(function() {
		if ( $( this ).hasClass( "active" ) ) { 
			$("html, body").animate({scrollTop:0},1500);
		}
	});
}

function removeCookieBox() {
	var cookieBox = document.getElementsByClassName('box--cookie')[0];
	if ( getCookie( 'displayCookieConsent' ) !== 'y' ) {
		cookieBox.style.display = 'block';
		cookieBox.addEventListener('click', function() {
			this.parentNode.removeChild(this);
			setCookie( 'displayCookieConsent', 'y', 365 );
			console.log('CookieSet');
		});
	}else{
		
	}
}

function backtotopScroll() {
	$scrollTop = $(document).scrollTop();
	var $responWinHeight;
	if ($device=='md') {
		$responWinHeight = $winHeight - 50;
		if ($scrollTop>=$responWinHeight) {
			$('.backtotop').addClass('active');
		} else{
			$('.backtotop').removeClass('active');
		}
	} else if ($device=='lg'){
		$responWinHeight = $winHeight - 60;
		if ($scrollTop>=$responWinHeight) {
			$('.backtotop').addClass('active');
		} else{
			$('.backtotop').removeClass('active');
		}
	} 
}

function showLeaderBoardScroll() {
	$scrollTop = $(document).scrollTop();
	var $responWinHeight;
		$responWinHeight = $winHeight/2;
		if ($scrollTop>=$responWinHeight) {
			if($('.leaderboard').hasClass('no_display')) 
				{}
			else
			{
				$('.leaderboard').addClass('active');
			}
		} 
}

function setCookie( e, t, n ) {
	var r = new Date(),
		i = new Date();
	i.setTime( r.getTime() + n * 24 * 60 * 60 * 1e3 );
	document.cookie = e + '=' + encodeURIComponent( t ) + ';expires=' + i.toGMTString() + ';domain='+id_name_ga+';path=/'; // CHANGE DOMAIN
}

function getCookie( e ) {
	var t = document.cookie,
		n, r, i;
	e = e + '=';
	for ( r = 0, c = t.length; r < c; r++ ) {
		i = r + e.length;
		if ( t.substring( r, i ) == e ) {
			cookEnd = t.indexOf( ';', i );
			if ( cookEnd == -1 ) {
				cookEnd = t.length;
			}
			return decodeURIComponent( t.substring( i, cookEnd ) );
		}
	}
	return null;
}



function init(){

}

function unfold_ventures(){
	$('.footer-ventures').click(function() {
			$('.sites_selector, .footer-right').toggleClass('active-on');
	});
}

function checkWindow() {
	if ($winWidth <= 768) {
		$('.menu').addClass('fixed');
	};
}
function goSticky() {
	$scrollTop = $(document).scrollTop();
	var $responWinHeight;
	if ($device=='md') {
		$responWinHeight = $winHeight - 50;
		if ($scrollTop>=$responWinHeight) {
			$('.menu, .menu-wrapper').addClass('fixed');
			$('.backtotop').addClass('active');
		} else{
			$('.menu, .menu-wrapper').removeClass('fixed')
			$('.backtotop').removeClass('active');
		}
	} else if ($device=='lg'){
		$responWinHeight = $winHeight - 60;
		if ($scrollTop>=$responWinHeight) {
			$('.menu, .menu-wrapper').addClass('fixed');
			$('.backtotop').addClass('active');
		} else{
			$('.menu, .menu-wrapper').removeClass('fixed');
			$('.backtotop').removeClass('active');
		}
	}
	
}


function goSwiperFullsingle() {
var galleryslider =  document.getElementById('swiper-container');
	if (typeof(galleryslider) != 'undefined' && galleryslider != null)
	{
	    var swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        slidesPerView: 1,
	        paginationClickable: true,
	        spaceBetween: 30,
	        loop: true
	    });
	}
}


function goSwiperFull() {
	var swiper = new Swiper('.swiper-container-test', {
        spaceBetween: 0,
        centeredSlides: true,
        autoplay:  {
		    delay: 5000,
		  },
        loop: true, 
    });
    $('.cover').css('width', $winWidth);
}



function goAnimCharg() {
	$(".store, .link-logo, .menu-wrapper").delay(400).queue(function(next){
    	$(this).addClass('start');
	    next();
	});
}

/** VIEWPORT **/
function isOnScreen(el){
	if (!$(el).hasClass("no_reload")) {
		if((el.attr("format") != 'Inarticle') && (el.attr("formatMOB") != 'MInarticle') && ((el.attr("viewport_time") && (el.attr("format") || el.attr("formatMOB")) ) /*&& (el.attr("format") != 'TopLarge')*/) || ((el.attr("viewport_time") && (el.attr("format") || el.attr("formatMOB")) ) && (el.attr("position") == 'gallerie'))  ){
			if(country_code_ads == 'BE' || country_code_ads == '')
			{
				var win = $(window);
				var viewport = {
				top : win.scrollTop(),
				left : win.scrollLeft()
				};
				viewport.right = viewport.left + win.width();
				viewport.bottom = viewport.top + win.height();

				var bounds = el.offset();
				bounds.right = bounds.left + el.outerWidth();
				bounds.bottom = bounds.top + el.outerHeight();
				if((!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom))){
					var time = el.attr('viewport_time');
					var ads_number = el.attr('ads_number');
					time = parseInt(time) + 1;
					el.attr('viewport_time',time);
					if(time > 15 && ads_number <= 3){ 
						ads_number = parseInt(ads_number) + 1;
						el.attr('ads_number',ads_number); 
						el.attr('viewport_time',1);
						el.removeAttr("loaded");
					}
				}
			}
		}
	}
};




function postscribe_ads(item){
		var wt = $(window).scrollTop();    //* top of the window
		var wb = wt + $(window).height();  //* bottom of the window
		var ot = item.offset().top - 200;  //* top of object (i.e. advertising div)
	    var ob = ot + item.height(); //* bottom of object	
			if(wt<=ob && wb >= ot && (!item.attr("loaded")) || (!item.attr("loaded") && ot <= 0) || (!item.attr("loaded") && item.attr("id") == 'halfpage-gallery')){
			var tempsEnMs = Date.now();
			var ads_number = item.attr('ads_number');
			if(typeof ads_number !== typeof undefined && ads_number !== false) {}
			else{
				ads_number = 1;
				item.attr("ads_number",1);
			}
			item.attr("dateload",tempsEnMs);
			item.attr("loaded",true);	
			item.attr("viewport_time",1);

			// BELGIQUE PAYS
			item.empty();
			if(window.innerWidth > 955){
					if(item.data("format") != ''){
						console.log('Loading : ' + item.attr("id"));
						postscribe('#'+item.attr("id"),'<div class="ads-block '+item.data("format")+'" id="'+item.data("refad")+'"><script type="text/javascript">adhese.tag({ format: "'+
		      			item.data("format")+'", publication:"'+lang_identifiantPebble+'", location: "'+adpositionpebble+'", position: "'+
		      			item.data("position")+'",categories:["'+adpositionpebble_categories+'"]});<\/script>');
					}
			}else{
				//console.log("Load ads Normal Mobile");
		      		if(item.data("formatmob") != ''){
		      			console.log('Loading Mobile : ' + item.attr("id"));
		      			postscribe('#'+item.attr("id"),'<div class="ads-block '+item.data("formatmob")+'" id="'+item.data("refadmob")+'"><script type="text/javascript">adhese.tag({ format: "'+
		      			item.data("formatmob")+'", publication:"'+lang_identifiantPebble+'", location: "'+adpositionpebble+'", position: "'+
		      			item.data("position")+'",categories:["'+adpositionpebble_categories+'"]});<\/script>');
		      		}
			}
		}
	}


function ads_blocker(item){
	console.log('LOADED AD BLOCKER');
	if(window.innerWidth > 955){
		if(item.data("format") != ''){
			if(item.data("format") == 'TopLarge'){
				$(item).html('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_lb_src + '"/></a>');
			}else if((item.data("format") == 'MiddleLarge' || item.data("format") == 'Middle')){
				$(item).html('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_imu_src + '"/></a>');
			}
		}
	}else{
		if(item.data("formatmob") != ''){
			if(item.data("formatmob") == 'MOB640x150'){
				$(item).html('<a target="_blank" href="' + autopromo_link_src + '"><img src="'+ autopromo_mlb_src +'"/></a>');
			}else if((item.data("formatmob") == 'MMR')){
				$(item).html('<a target="_blank" href="' + autopromo_link_src + '"><img src="' + autopromo_imu_src + '"/></a>');
			}

		}
	}
}


/****** AJAX CONTENT ******/

/**  Fonction Multipage   **/
function load_article_scroll(){
	var wt = $(window).scrollTop();    //* top of the window
	var wb = wt + $(window).height();  //* bottom of the window
	$(".related-posts").each(function(){
		var ot = $(this).offset().top;  //* top of object (i.e. advertising div)
    	var ob = ot + $(this).height(); //* bottom of object		
		if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			console.log('Load one more article');
			$(this).attr("loaded",true);	
			ajax_do_content_more_article();
		}
	});
}

function google_analytics_load(){
    // Google Analytics Init //
    console.log("LOAD Google Analytics");
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', lang_identifiantGoogleAnalytics, id_name_ga);
	ga('require', 'displayfeatures');
	ga('require', 'linkid', 'linkid.js');

}

function google_analytics_init(){
	console.log("INIT Google Analytics");

	if(adpositiontype == ' (Article)'){
		ga('send', 'pageview', {
			'dimension1': id_author,
			'dimension2': adposition + adpositiontype,
			'dimension3': id_post + ' - '+ adposition + ' - ' + article_title + ' - ' + id_author,
			'dimension4': adpositionbis + adpositiontype,
			'dimension5': adpositionter + adpositiontype
		});
	}
	else{
		ga('send', 'pageview', {
			'dimension2': adposition + adpositiontype,
			'dimension4': adpositionbis + adpositiontype,
			'dimension5': adpositionter + adpositiontype
		});				
	}	
}

function generate_pageview(last_object,index,type){
	last_id = last_object.data("id");
	user_login = last_object.data("author");
	cat = last_object.data("adposition").toUpperCase();
	title = last_object.data("title");
	subcat = last_object.data("adpositionbis");
	tag = last_object.data("tag");
	link = last_object.data("link");
	console.log('Generate PV INDEX : '+ index);
	index = '#p-'+index; 
	//window.history.pushState("Details", "Title", link + index);	

	ga('set', 'location', link);
	ga('send', 'pageview', {
	'dimension1': user_login,
	'dimension2': cat  + ' (Article)',
	'dimension3': last_id + ' - '+ cat + " - " + title + " - " + user_login,
	'dimension4': subcat + ' (Article)',
	'dimension5': tag + ' (Article)',
	'dimension7': type,
	});		
}

/**  Fonction Multipage   **/
	function multipage_scroll(item){
		var wt = $(window).scrollTop();    //* top of the window
		var wb = wt + $(window).height();  //* bottom of the window
		$(item).find('.segment-multipage').each(function(){
			var ot = $(this).offset().top;  //* top of object (i.e. advertising div)
	    	var ob = ot + $(this).height(); //* bottom of object		
			if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			$(this).attr("loaded",true);	
			current_index = $(this).find(".current-multipage").html();
			if(current_index != '' && typeof(current_index) != "undefined"){
				generate_pageview(item,current_index,'multipage');
					if(window.innerWidth > 955){
					console.log("Load multipage Dekstop");
					}else{
					console.log("Load multipage Mobile");
					}
				}
			}
		});
			$(item).find('.gallery-compteur').each(function(){
			var ot = $(this).offset().top;  //* top of object (i.e. advertising div)
	    	var ob = ot + $(this).height(); //* bottom of object		
			if(!$(this).attr("loaded") && wt<=ob && wb >= ot){
			$(this).attr("loaded",true);	
			generate_pageview(item,'','gallerie');
				if(window.innerWidth > 955){
					console.log("Load Gallerie Dekstop");
				}else{
					console.log("Load Gallerie Mobile");
				}
				
			}
		});
	}


function ajax_do_content_more_article(){
		//more_article = $( ".article" ).data("more");
		tag_article = $( ".article" ).data("tag");
		cat_article = $( ".article" ).data("cat");
		console.log('TAG TESTING SCROLL' + tag_article);
		console.log('CAT TESTING SCROLL' + cat_article);
		var excluded_id = [];
		var numItems = $('.article').length
		if(numItems < 3) {
			$(".article").each(function(){
				excluded_id.push($( this ).data("id"));
			});

			/** CONFLIT ADD tag: tag_article au data */
					$.ajax({
						url: ajax_search.ajaxurl,
						type: 'post',
						data: { action: 'ajax_load_article',cat:cat_article,tag:tag_article,excluded_id: excluded_id },
						beforeSend: function() {
						
						},
						success: function( html ) {
							$('.loading').remove();
							if(html != ''){
								$('.container-articles').append( html );
								last_object = $(".article").last();
								//load_total_multipage(last_object);
								//init_slider_gallery();
								generate_pageview(last_object,0,'article');
								init_js_gallery();
								//triggerInernalLinkClicks();
							}else{
								$('.container-articles').append( html );
							}
						},
						error: function (xhr) {
							console.log('error more content'); 
			   			} 
				})
		}	   
}

function init_ligatus(){ 
	console.log('Loading : Ligatus');
	if(lang_identifiant == 'FR'){
		$('.ligatus-div').html('Ailleurs sur le web');
		script_ligatus = '<script type="text/javascript" src="https://a-ssl.ligatus.com/?ids=88893&t=js&s=1"></script>';
		postscribe('#lig_decoidees_smartbox_fr',script_ligatus);
	}else if(lang_identifiant == 'NL'){
		$('.ligatus-div').html('Elders op het web');
		script_ligatus = '<script type="text/javascript" src="https://a-ssl.ligatus.com/?ids=88925&t=js&s=1"></script>';
		postscribe('#lig_actiefwonen_smartbox_nl',script_ligatus);	
	}
}

function init_js_gallery(){
	        var mySwiper = new Swiper ('.swiper-container', {
          // Optional parameters
          calculateHeight: true,
          direction: 'horizontal',
          loop: true,
          resistance: '100%',
          speed: 500,
    	          // If we need pagination
      	  pagination: '.swiper-pagination',
      	    
      	    // Navigation arrows
      	  nextButton: '.swiper-button-next',
      	  prevButton: '.swiper-button-prev',
      	
      	  paginationType: 'fraction',

    	  //hashnav: true,
    	  onSlideChangeStart: function (mySwiper) {
    	        // start Swiper slide
    	      var urlslide = jQuery(mySwiper.slides[mySwiper.activeIndex]).attr("data-url");
    	      document.location.hash = urlslide;
              if(mySwiper.activeIndex != 1){
              console.log("CHANGE IMAGE SLIDER");
              //lazyload();
              ga('send', 'pageview', { 'page': location.pathname + location.search + location.hash});
              //ga('newTrackerFR.send', 'pageview', { 'page': location.pathname + location.search + location.hash});
              }
              
    	    },
        }) 

    var positionfleche = parseInt(jQuery('.swiper-home-pic').height());
    var positioncompteur = positionfleche + 15;
    positionfleche = positionfleche/2;
    console.log('test'+positionfleche);
    jQuery('.swiper-button-prev').css('top',positionfleche+'px');
    jQuery('.swiper-button-next').css('top',positionfleche+'px');
    jQuery('.swiper-pagination').css('top',positioncompteur+'px');


    jQuery('.swiper-home-pic').click(function() {
      jQuery('.swiper-home-pic').css('display','none');
      jQuery('.swiper-container-shop').css('z-index','1');
      jQuery('.swiper-container-shop').css('opacity','1');
      jQuery('.swiper-container-shop').css('position','relative');
    });
}

function testing_passive_listener(){
	/*elementsArray = $('.ads');
	console.log('view_ads');
	elementsArray.forEach(function(elem) {
    elem.addEventListener("touchstart", function() {
       	console.log('view_ads');
   		});
	});*/
}

function newsletters_popup(){
		var visited = getCookie('visited_news');
		var display = getCookie('display_news');
		console.log('visitedBef : '+ visited);
        if(visited == null ){ visited = 3; setCookie( 'visited_news', '3', 15 ); }
        console.log('visited : '+ visited);
        console.log('display_news : '+ display);
        if(display == 0 || display == null ){
          if (visited == 0) {
          	  setCookie( 'visited_news', visited , 15 );
	          setTimeout(function () {
	          	console.log('displaynews');
	            setCookie( 'display_news', 1 , 15 );
	            $(".full-modal-news").toggleClass('active');
				$(".full-modal-news-input" ).focus();
	          }, 3000);
	        
     	 	}
          else {
          visited--;
          console.log('visited  -- : '+ visited);
          setCookie( 'visited_news', visited , 15 );
          }
        }
  }


/** Facebook tracking fct **/
function trackingFacebook_fct(){
	/** SINGLE **/
	if($('body').hasClass('single')){
		fbq('track', 'ViewContent', {title: article_title,url: article_link });
		fbq('track', 'ViewCategory', {title: article_category });
		fbq('track', 'ViewTag', {title: article_tag });
	}
	if($('body').hasClass('category')){
	/** CATEGORY **/
		fbq('track', 'ViewCategory', {title: article_category });
	}
	/** TAG **/
	if($('body').hasClass('tag')){
		fbq('track', 'ViewTag', {title: article_tag });
	}
	/** SEARCH */
	/* fbq('track', 'Search', {search_string: '<?php echo $_GET['s'] ?>'});*/
}

/** Facebook tracking **/
function facebook_tracking(){
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', id_facebook_pixel); // Insert your pixel ID here.
		fbq('track', 'PageView');

		trackingFacebook_fct();
}


$(document).ready(function(){
	//** LOADING GLOABAL **/
	newsletters_popup();
	testing_passive_listener();

	google_analytics_load();
	google_analytics_init();
	facebook_tracking();
	$('.ads-first').each(function(){  postscribe_ads($(this)); });

	//init_js_gallery();
	checkWindow();
	unfold_ventures();
	//goSwiperFull();
	goAnimCharg();

	/** LOADING HOME **/
	if($('body').hasClass('home')){
		goSwiperFull();
	};	

	if($('body').hasClass('single')){
		//init_ligatus();
		//goSwiperFullsingle();
	};	

		/****** CACAAAA ***/
			checkWindowCommun();
			goSearch();
			backtotop();
			removeCookieBox();
			goNews();
			$('.footer-cover').attr("src",$('#cover_ori_FR').attr("src"));


		var gallerymos =  document.getElementById('gallery-mos');
		if (typeof(gallerymos) != 'undefined' && gallerymos != null)
			{
			$(".fancybox").fancybox();
			}

		/****** CACAAAA ***/


	setTimeout(function() {console.log('Init ads after 1s isOnScreen');	
	  	window.setInterval(function(){
	  		$('.ads').each(function(){ isOnScreen($(this));  postscribe_ads($(this)); });
	  	}, 1500 );
 }, 1000);

});

$(document).scroll(function(){
	last_object = $(".article").last();


    goSticky();
    backtotopScroll();
	showLeaderBoardScroll();

	if($('body').hasClass('single')){
		load_article_scroll();
		multipage_scroll(last_object);
	};		

});

$(window).resize(function(){
	// reload de la page
	// window.location.href = window.location.href;

});

