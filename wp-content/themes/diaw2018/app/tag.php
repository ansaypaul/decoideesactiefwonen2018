<?php get_header(); ?>
	<main class="main">
		<div class="container-main">
			<div class="full-content">
				<h1 class="category">
					<?php single_cat_title(); ?>
				</h1>
				<div class="sub-category">
				</div>
			</div>
			<div class="content">
				<?php $latest_cat_post = new WP_Query( array('post_type' => 'post','posts_per_page' => 10,'paged'=>$paged,'tag__in' => array($tag_id)));			
					if( $latest_cat_post->have_posts() ) :
						$i = 0; 
						while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
								$i++;
								get_template_part( 'templates/template_category' );
						endwhile;
					endif;
				?>
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</main>
<?php get_footer(); ?>