document.addEventListener('DOMContentLoaded',function(){
	var body = document.body,
		ads = [];
	var width = body.clientWidth,
		isAttachment = body.classList.contains('attachment'),
		location = body.classList.contains('home') ? "homepage" : "others";

	function Ad(el) {
		this.parent = el;
		this.format = el.getAttribute('format');
		this.position = el.getAttribute('position');
		this.hasClose = el.getAttribute('close');
		this.init();
	}

	Ad.prototype = {
		config: advertising.config,
		init: function () {
			if(this.config[this.format])
			if(advertising.type === "dfp") this.setDFPAd(this.format); // advertising.type est définis lors de l'appel du Plugin (advertising.php)
			else this.setPebbleAd(this.format,this.position);
			if(this.hasClose === "true") this.setClose();
		},
		setDFPAd: function (format) { // A revoir / corriger lorsqu'on mettra en place une régie pub via dfp
			var ad = googletag.pubads(),
				sizes = isAttachment && format['attachment'] ? format['attachment'] : format['default'],
				targeting = format['targeting'],
				size = 0;
			for (var _size in sizes) if(width >= size) size = _size;
			ad.definePassback(format.tag, sizes[size]).setTargeting('kw', location);
			if(targeting && targeting[size]) ad.setTargeting(targeting[size][0], targeting[size][1]);
			return ad;
			// ad.display();
		},
		setPebbleAd: function (format,position) {
			var format = this.config[format], 			
				script = document.createElement('script'),
				div_parent = document.createElement('div'),
				div = document.createElement('div'),
				inner = document.createElement('div'),
				size = 0;
			var	sizes = isAttachment && format['attachment'] ? format['attachment'] : format['default'];
			console.log(sizes);
			for (var _size in sizes) if(width >= parseInt(_size)) size = _size;
			console.log('Load ad :'+format['attachment'] +sizes[size]['format']);
			if(position == 0){
			script.text = "postscribe('#" + sizes[size]['id'] + position +"', \"<script>adhese.tag({ format: '" + sizes[size]['format'] + "', publication:'" + this.config['publication'] + "', location: '" + location + "'});<\/script>\");";
			div_parent.setAttribute("id", "pebble"+ position);
			div.setAttribute("id", sizes[size]['id'] + position);
			}
			else{
			script.text = "postscribe('#" + sizes[size]['id'] + position  + "', \"<script>adhese.tag({ format: '" + sizes[size]['format'] + "', publication:'" + this.config['publication'] + "', location: '" + location + "', position: '" + position + "'});<\/script>\");";
			div_parent.setAttribute("id", "pebble"+ position);
			div.setAttribute("id", sizes[size]['id'] + position);
			}
			div.className = "ad__inner " +  sizes[size]['id'];
			div.appendChild(script);
			div_parent.appendChild(div); 
			//inner.appendChild(script);
			this.parent.appendChild(div_parent);
		},
		setClose: function () {
			span = document.createElement('span');
			span.className = "icon icon-close close--js";
			//this.parent.getElementsByClassName('ad__inner')[0].appendChild(span);
		}
	}

	var _ads = document.getElementsByClassName('ad');

	for (var i = 0; i < _ads.length; i++) {
		ads.push(new Ad(_ads[i]));
	}
	console.log('Ad are LOADED ('+advertising.config.type+')');
});
