<div class="fav">
	<h3 class="_color"><?php _e( "Les favoris", "html5blank" ); ?></h3>
	<!-- // Non classé ID=1 // Slider ID=2044 -->
	<?php 
	$query = new WP_Query( array ( 'posts_per_page' => 3, 'offset' => $offset, 'ignore_sticky_posts' => 1, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC','date_query' => array(array('after' => '2 week ago'))));?>
	<?php  if ( $query->post_count >= 3)  { ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				<article>
				<?php foreach((get_the_category()) as $category) { ?>
					<?php if (!empty( $category ) && ($category != 'slider') && $category->parent == 0 ) { ?>
						<a class="fav-category _color _border" href="<?php echo get_category_link($category->term_id); ?>">
							<?php echo esc_html( $category->name ); ?>
						</a>
						<?php 	
						break;
						}	?>
				<?php } ?>
					<a href="<?php the_permalink(); ?>">
						<h4><?php the_title(); ?></h4>
					</a>
				</article>
			<?php wp_reset_postdata(); ?>
		<?php endwhile; ?>
	<?php } ?> 
</div>