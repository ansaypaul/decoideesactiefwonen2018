<?php
/*
Template Name: mon-profil NL
*/
?>

<?php get_header(); ?>
<?php 

$username='elle/WebServices_Paul';
$password='87BOI8HC';
$URL='https://www.actito.be/ActitoWebServices/ws/v4/entity/Ventures/table/ventures/profile?search=Token%3D'.$_GET['Token'];
$fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_STDERR, $fp);
curl_setopt($ch, CURLOPT_URL,$URL);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
$result  = curl_exec($ch);

$days = array('01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31');
$months = array('01:Januari','02:Februari','03:Maart','04:April','05:Mei','06:Juni','07:Juli','08:Augustus','09:September','10:Oktober','11:November','12:December');
$years = array_combine(range(date("Y"), 1930), range(date("Y"), 1930));
$subscriptions_init = array('Marie Claire','Actief Wonen Partenaire', 'ELLE', 'Life Magazine','Psychologies','Actief Wonen');

if (empty($result)) {
    // some kind of an error happened
   /* echo "No server responded<br />";*/
    die(curl_error($ch));
    curl_close($ch); // close cURL handler
} else {
    $info = curl_getinfo($ch);
    curl_close($ch); // close cURL handler
    /*echo "Server responded: <br />";*/
    if (empty($info['http_code'])) {
            die("No HTTP code was returned");
    } else {
      
       /* echo "The server responded: <br />";
        echo $info['http_code'];
        echo $result ;*/
    }

}

$profil = json_decode($result);


if(isset($_POST)  && !empty($_POST))
{
  $lastname = $_POST['attributes'][0]['value'];
  $firstName = $_POST['attributes'][1]['value'];
  $addressBox = $_POST['attributes'][2]['value'];
  $addressLocality = $_POST['attributes'][3]['value'];
  $addressNumber = $_POST['attributes'][4]['value'];
  $gsmNumber = $_POST['attributes'][5]['value'];
  $emailAddress = $_POST['attributes'][7]['value'];
  $addressPostalCode = $_POST['attributes'][8]['value'];
  $addressStreet = $_POST['attributes'][9]['value'];
  $birthDate = $_POST['attributes'][10]['value'];
  //$civilState = $_POST['attributes'][11]['value'];
  $sex = $_POST['attributes'][11]['value'];

  $day_profil = $_POST['dob_day'];
  $month_profil = $_POST['dob_month'];
  $year_profil = $_POST['dob_year'];

  $subscriptions_profil = $_POST['subscriptions'];
  /*$motherLanguage =
  $civilState =
  $creationSource =*/
}
else 
{
  $lastname = $profil->profiles[0]->attributes[3]->value;
  $firstName = $profil->profiles[0]->attributes[4]->value;
  $addressBox = $profil->profiles[0]->attributes[14]->value;
  $addressLocality = $profil->profiles[0]->attributes[9]->value; 
  $addressNumber = $profil->profiles[0]->attributes[7]->value; 
  $gsmNumber = $profil->profiles[0]->attributes[12]->value;
  $emailAddress = $profil->profiles[0]->attributes[5]->value;
  $addressPostalCode = $profil->profiles[0]->attributes[8]->value;
  $addressStreet = $profil->profiles[0]->attributes[6]->value; 
  $birthDate = $profil->profiles[0]->attributes[10]->value; 
 // $civilState = $profil->profiles[0]->attributes[16]->value; 
  $sex = $profil->profiles[0]->attributes[19]->value; 

  foreach ($profil->profiles[0]->subscriptions as $subscription) {
    $subscriptions_profil[] = $subscription->name;
  }

  $day_profil = explode('/',$birthDate)[0];
  $month_profil = explode('/',$birthDate)[1];
  $year_profil = explode('/',$birthDate)[2];
  /*$motherLanguage =
  $civilState =
  $creationSource =*/

}

?>
<section class="main-wrapper">
  <main class="main">
    <div class="container-main">
      <div class="full-content">
        <h1 class="page-title">Vull uw profiel in</h1>
      </div>
      <div class="main-content">
        <form method="POST" role="form" id="profil_news"> 
          <input type='hidden' name='attributes[0][name]' value='lastname'>
          <input type='hidden' name='attributes[1][name]' value='firstName'>
          <input type='hidden' name='attributes[2][name]' value='addressBox'>
          <input type='hidden' name='attributes[3][name]' value='addressLocality'>
          <input type='hidden' name='attributes[4][name]' value='addressNumber'>
          <input type='hidden' name='attributes[5][name]' value='gsmNumber'>
          <input type='hidden' name='attributes[7][name]' value='emailAddress'>
          <input type='hidden' name='attributes[8][name]' value='addressPostalCode'>
          <input type='hidden' name='attributes[9][name]' value='addressStreet'>
          <input type='hidden' name='attributes[10][name]' value='motherLanguage'>
          <input type='hidden' name='attributes[11][name]' value='sex'>
          <input type='hidden' name='attributes[13][name]' value='Token'>
          <input type='hidden' name='Token' value='<?php echo $_GET['token']; ?>'>
          <input type='hidden' name='attributes[14][name]' value='creationSource'>
          <input type='hidden' name='attributes[14][value]' value='DecoIdees-NL'>
          <input type='hidden' name='attributes[15][name]' value='motherLanguage'>
          <input type='hidden' name='attributes[15][value]' value='NL'>
          <input type='hidden' name='subscriptions[]' value='Actief Wonen'><label class="label-checkpart"></label>
          <!--<input type='hidden' name='attributes[12][name]' value='birthDate'>-->
          <!-- <img src="http://www.psychologies.be/img/newsletter_enveloppe.png" alt="" class="news-env"> -->
          <div class="civil">
              <input <?php if($sex == 'M'){ echo'checked'; } ?> name="attributes[11][value]" id="civilMonsieur" value="M"  type="radio"><label for="civilMonsieur">Dhr.</label>
              <input <?php if($sex == 'F'){ echo'checked'; } ?> name="attributes[11][value]" id="civilMadame" value="F" type="radio"><label for="civilMadame">Mw.</label>
          </div> 

          <div class="required _color">Verplichte velden*</div>

          <div>
            <input name='attributes[1][value]' placeholder="Voornaam*" title='Voornaam' value='<?php echo $firstName ?>' required>
          </div>

          <div>
            <input name='attributes[0][value]' placeholder="Naam*" title='Naam' value='<?php echo $lastname ?>' required>
          </div>

          <div>
            <input name='attributes[7][value]' value='<?php echo $emailAddress; ?>' placeholder="Email*" title='Email*' readonly required>
          </div>
        
          <div class="select-date">
            <select id="form_day" name="dob_day">
            <option value="-">-</option>
            <?php foreach ($days  as $day) 
            {
              if($day_profil == $day){
              echo '<option selected value="'.$day.'">'.$day.'</option>';
              }else{
              echo '<option value="'.$day.'">'.$day.'</option>';
              }
            }
            ?>
            </select>

             <select id="form_month" name="dob_month">
              <option value="-">-</option>
            <?php foreach ($months  as $month) 
            {
              if($month_profil == explode(':',$month)[0]){
              echo '<option selected value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
              }else{
              echo '<option value="'.explode(':',$month)[0].'">'.explode(':',$month)[1].'</option>';
              }
            }
            ?>
            </select>
             <select id="form_year" name="dob_year">
              <option value="-">-</option>
              <?php foreach ($years  as $year) 
              {
                if($year_profil == $year){
                echo '<option selected value="'.$year.'">'.$year.'</option>';
                }else{
                echo '<option value="'.$year.'">'.$year.'</option>';
                }
              }
              ?>
            </select>
          </div>

          <div>
            <input name='attributes[5][value]' value='<?php echo $gsmNumber ?>' placeholder="GSM"  title="GSM" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" required>
          </div>

          <div>
            <input name='attributes[9][value]' placeholder="Straat*" title="Straat" value='<?php echo $addressStreet  ?>' required>
          </div>

          <div>
            <input name='attributes[4][value]' placeholder="Straatnummer*" title='Straatnummer' value='<?php echo $addressNumber  ?>' required>
          </div>

          <div>
            <input name='attributes[2][value]' placeholder="Brievenbus" title='Brievenbus' value='<?php echo $addressBox ?>' >
          </div>

          <div>
            <input name='attributes[8][value]' placeholder="Postcode" title='Postcode' value='<?php echo $addressPostalCode ?>' required>
          </div>

          <div>
            <input name='attributes[3][value]' placeholder="Stad*" title='Stad' value='<?php echo $addressLocality ?>' required>
          </div>
        
          <input type='hidden' name='attributes[10][value]' value='fr'>
        <div class="mag"> 
          <div class="custom-part">
            <input type='checkbox' name='subscriptions[]' value='Actief Wonen Partenaire'
            <?php if(in_array( 'Actief Wonen Partenaire',$subscriptions_profil)){echo 'checked';}?> 
            >
          </div>
        <span class="check-label">Ik wil geschenken, kortingsbonnen en aanbiedingen van partners Marie Claire ontvangen.</span>
          <h2>Ik wens in te schrijven voor meer nieuwsbrieven:</h2>
        <div class="wrap-card">
                <div class="custom-cb">
                  <input type='checkbox' class='input-cb' name='subscriptions[]' value='ELLE'
                  <?php if(in_array( 'ELLE',$subscriptions_profil)){echo 'checked';}?> 
                  ><label class="label-checkbox"></label>
                </div>
                <div class="card">
                  <div class="card-content">
                    <img src="http://www.psychologies.be/img/elle_website_screen.png" alt="Elle website" class="news-web">
                    <img src="http://www.psychologies.be/img/VenturesAllLogos_Elle.be.png" alt="ELLE logo" class="news-logo">
                  </div>
                </div>
              </div>
              
              <div class="wrap-card">
                <div class="custom-cb">
                  <input type='checkbox' class='input-cb' name='subscriptions[]' value='Psychologies'
                  <?php if(in_array( 'Psychologies',$subscriptions_profil)){echo 'checked';}?> 
                  >
                </div>
                <div class="card">
                  <div class="card-content">
                    <img src="http://www.psychologies.be/img/psycho_website_screen.png" alt="Psychologie website" class="news-web">
                    <img src="http://www.psychologies.be/img/VenturesAllLogos_Psychologies.png" alt="Marie Claire logo" class="news-logo">
                  </div>
                </div>
              </div>

              <div class="wrap-card">
                <div class="custom-cb">
                  <input type='checkbox' class='input-cb' name='subscriptions[]' value='Life Magazine'
                  <?php if(in_array( 'Life Magazine',$subscriptions_profil)){echo 'checked';}?> 
                  >
                </div>
                <div class="card">
                    <div class="card-content">
                    <img src="http://www.psychologies.be/img/life_website_screen.png" alt="Life website" class="news-web">
                    <img src="http://www.psychologies.be/img/VenturesAllLogos_LifeMagazine.png" alt="Life logo" class="news-logo">
                  </div>
                </div>
              </div>
              <div class="wrap-card">
                <div class="custom-cb">
                  <input type='checkbox' class='input-cb' name='subscriptions[]' value='Marie Claire'
                  <?php if(in_array( 'Marie Claire',$subscriptions_profil)){echo 'checked';}?> 
                  >
                </div>
                <div class="card">
                  <div class="card-content">
                    <img src="http://www.psychologies.be/img/mc_website_screen.png" alt="Marie Claire website" class="news-web">
                    <img src="http://www.psychologies.be/img/VenturesAllLogos_MarieClaire.png" alt="Marie Claire logo" class="news-logo"> 
                  </div>
                </div>
              </div>

          </div>
          <span class="check-label">Ik wil geschenken, kortingsbonnen en aanbiedingen van partners Marie Claire ontvangen.</span>
          <input class="_background" type='submit' value='Registreer'/>
          <?php if(isset($_POST)  && !empty($_POST)) {echo "<h2 style='color:black' class='entry-title'>We danken je voor je inschrijving, binnenkort ontvang je de nieuwsbrieven van Actief wonen!</h2>";}?>
          </form>
      </div>
      <?php get_sidebar(); ?>
    </div>
  </main>
<?php get_footer(); ?>

<?php
if(isset($_POST)  && !empty($_POST))
  {
  $JSON_STRING = '';
  $nb_attributes_i = 0;
  $nb_attributes = count($_POST['attributes']);

  if($nb_attributes != 0)
  {
    $JSON_STRING .= '{"attributes":[';
    foreach ($_POST['attributes'] as $attributes) {
    $nb_attributes_i++;
    if($attributes['value'] != ''){
      if($nb_attributes_i != $nb_attributes)
        {
        $JSON_STRING .= '{"name":"'.$attributes['name'].'","value":"'.$attributes['value'].'"},';
        }
        else
        {
        $JSON_STRING .= '{"name":"'.$attributes['name'].'","value":"'.$attributes['value'].'"},';
        }
      }
    }
  }

  if((isset($_POST['dob_day']) && $_POST['dob_day'] != '-') && (isset($_POST['dob_month']) && $_POST['dob_day'] != '-')  && (isset($_POST['dob_year']) && $_POST['dob_day'] != '-'))
  {
   
  $JSON_STRING .= '{"name":"birthDate","value":"'.$_POST["dob_day"].'/'.$_POST["dob_month"].'/'.$_POST["dob_year"].'"},';

  }

  if(isset($_POST['Token'])) 
  {
   
  $JSON_STRING .= '{"name":"blank","value":""}';

  }

  $JSON_STRING .= ']';

  if(isset($_POST['subscriptions'])){
  $nb_subscriptions_i = 0;
  $nb_subscriptions = count($subscriptions_init);
  if(count($nb_subscriptions) != 0){
    $JSON_STRING .= ',"subscriptions":[';
    foreach ($subscriptions_init as $subscriptions) {
      $nb_subscriptions_i++;
          if($nb_subscriptions_i != $nb_subscriptions)
            {
              if(in_array($subscriptions, $_POST['subscriptions']))
              {
                  $JSON_STRING .= '{"name":"'.$subscriptions.'","subscribe":"true"},';
              }
                else
              {
                  $JSON_STRING .= '{"name":"'.$subscriptions.'","subscribe":"false"},';
              }
            }
            else
            {
             if(in_array($subscriptions, $_POST['subscriptions']))
              {
                  $JSON_STRING .= '{"name":"'.$subscriptions.'","subscribe":"true"}]';
              }
                else
              {
                  $JSON_STRING .= '{"name":"'.$subscriptions.'","subscribe":"false"}]';
              }
            }
        }
    }
  }
  $JSON_STRING .= '}';

  $username='elle/WebServices_Paul';
  $password='87BOI8HC';
  $URL='https://www.actito.be/ActitoWebServices/ws/v4/entity/Ventures/table/ventures/profile';
  $fp = fopen(dirname(__FILE__).'/errorlog.txt', 'w');
  /*echo $JSON_STRING;*/

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Accept: application/json'));
  curl_setopt($ch, CURLOPT_VERBOSE, true);
  curl_setopt($ch, CURLOPT_STDERR, $fp);
  curl_setopt($ch, CURLOPT_URL,$URL);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,TRUE);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
  curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $JSON_STRING ); 
  $result  = curl_exec($ch);


  if (empty($result)) {
      // some kind of an error happened
      /*echo "No server responded<br />";*/
      die(curl_error($ch));
      curl_close($ch); // close cURL handler
  } else {
      $info = curl_getinfo($ch);
      curl_close($ch); // close cURL handler
     /*echo "Server responded: <br />";*/
      if (empty($info['http_code'])) {
              die("No HTTP code was returned");
      } else {
        
          // echo results
          /*echo "The server responded: <br />";
          echo $info['http_code'];
          echo $result ;*/
      }

  }
}
?>
          </div>
          <?php get_sidebar(); ?>
        </div>
      </main>
    </section>
<?php get_footer(); ?>