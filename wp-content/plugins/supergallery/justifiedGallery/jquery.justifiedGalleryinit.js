jQuery(document).ready(function () {
        //initialize swiper when document ready  
        var mySwiper = new Swiper ('.swiper-container', {
          // Optional parameters
          calculateHeight: true,
          direction: 'horizontal',
          loop: true,
          resistance: '100%',
          speed: 500,
    	          // If we need pagination
      	  pagination: '.swiper-pagination',
      	    
      	    // Navigation arrows
      	  nextButton: '.swiper-button-next',
      	  prevButton: '.swiper-button-prev',
      	
      	  paginationType: 'fraction',

    	  //hashnav: true,
    	  onSlideChangeStart: function (mySwiper) {
    	        // start Swiper slide
    	      var urlslide = jQuery(mySwiper.slides[mySwiper.activeIndex]).attr("data-url");
    	      document.location.hash = urlslide;
              if(mySwiper.activeIndex != 1){
              console.log("CHANGE IMAGE SLIDER");
              //lazyload();
              ga('send', 'pageview', { 'page': location.pathname + location.search + location.hash});
              //ga('newTrackerFR.send', 'pageview', { 'page': location.pathname + location.search + location.hash});
              }
              
    	    },
        }) 

    var positionfleche = parseInt(jQuery('.swiper-home-pic').height());
    var positioncompteur = positionfleche + 15;
    positionfleche = positionfleche/2;
    console.log('test'+positionfleche);
    jQuery('.swiper-button-prev').css('top',positionfleche+'px');
    jQuery('.swiper-button-next').css('top',positionfleche+'px');
    jQuery('.swiper-pagination').css('top',positioncompteur+'px');


    jQuery('.swiper-home-pic').click(function() {
      jQuery('.swiper-home-pic').css('display','none');
      jQuery('.swiper-container-shop').css('z-index','1');
      jQuery('.swiper-container-shop').css('opacity','1');
      jQuery('.swiper-container-shop').css('position','relative');
    });
  });