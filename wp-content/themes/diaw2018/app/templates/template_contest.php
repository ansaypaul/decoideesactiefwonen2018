<!-- Contest -->
<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ $slug_concours = 'concours'; } else {  $slug_concours = 'wedstrijd'; }?>
<?php $query = new WP_Query( array ( 'posts_per_page' => 1, 'offset' => $offset, 'category_name'=> $slug_concours, 'ignore_sticky_posts' => 1, 'orderby' => 'rand')); ?>
<?php if ( $query->post_count >= 1)  { ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
		<div class="concours">
			<h3 class="_color"><?php _e( "Concours", "html5blank" ); ?></h3>
			<a href="<?php the_permalink(); ?>">
				<article>
					<figure>
						<?php the_post_thumbnail('d_news'); ?>
						<figcaption>
							<h4 class="_background"><?php the_title(); ?></h4>
						</figcaption>
					</figure>
				</article>	
			</a>
		</div>
	<?php wp_reset_postdata(); endwhile; ?>
<?php } else {
		if(is_home())
	{
?>
	<div class="concours">
		<h3 class="_color"><?php _e( "Concours", "html5blank" ); ?></h3>
		<p><?php _e( "Il n'y a pas de concours actuellement", "html5blank" ); ?>.</p>
	</div>
<?php
	}
}
?> 
