	</section>
		<footer id="footer">
			<div class="footer-insta">
				<div class="hashtag _color"><?php _e( "#decoidees", "html5blank" ); ?></div>
				 <?php echo do_shortcode( '[instagram-feed]' ); ?>
			</div>
			<div class="footer-wrapper _background">
				<div class="container-main">
					<div class="row">
						<div class="footer-div footer-left">
							<span class="left">
								<a href="https://www.viapress.be/magazine-deco-idees.html" target="_blank">
									<img src="<?php bloginfo("template_directory"); ?>/img/DI_Cover.jpg" class="footer-cover" alt="<?php _e( "Déco idées", "html5blank" ); ?> cover">
								</a>
							</span>
							<span class="right">
								<a href="http://www.viapress.be/3188/abonnement-magazine-deco-idees.html" target="_blank"><?php _e( "Abonnez-vous!", "html5blank" ); ?></a>
								<a href="http://www.viapress.be/3188/abonnement-magazine-deco-idees.html" class="tarif" target="_blank"><?php _e( "1 AN = 27,00€", "html5blank" ); ?></a>
								<p><?php _e( "Pour ne rien rater, inscrivez-vous à notre newsletter", "html5blank" ); ?></p>
								<span class="newsletter"><?php _e( "Recevez la newsletter", "html5blank" ); ?></span>
										<form method="POST" role="form" id="" action="<?php _e( "http://www.decoidees.be/newsletters/", "html5blank" ); ?>">
      			  						<input type="hidden" name="attributes[7][name]" value="emailAddress">

			      						<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
								        <input type='hidden' name='attributes[14][value]' value='DecoIdees-FR'>
								        <input type='hidden' name='attributes[15][name]' value='motherLanguage'>
								        <input type='hidden' name='attributes[15][value]' value='FR'>
								        <?php } else { ?>
								        <input type='hidden' name='attributes[14][value]' value='DecoIdees-NL'>
								        <input type='hidden' name='attributes[15][name]' value='motherLanguage'>
								        <input type='hidden' name='attributes[15][value]' value='NL'>
								        <?php }; ?>

			     					    <input type="hidden" name="Token" value="*">
			       						<input class="full-modal-news-input _border" type="email" name="footer_email" value="" placeholder="<?php _e( "Votre adresse e-mail", "html5blank" ); ?>" required>
		           						<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
							            <input type='hidden' name='subscriptions[]' value='Déco Idées'>
							            <?php } else { ?>
							            <input type='hidden' name='subscriptions[]' value='Actief Wonen'>
							            <?php }; ?>

								   <input type="submit" value="OK">
								</form>
							</span>
						</div>
						<div class="footer-div footer-center">
							<span class="commercial"><?php _e( "Contacts commerciaux:", "html5blank" ); ?></span>
								<a href="mailto:cli@editionventures.be">Catherine Limon</a>
								<a href="mailto:rma@editionventures.be">Rachel Macaluso</a>
								<br/>
							<span class="commercial"><?php _e( "Contacts rédactionnels:", "html5blank" ); ?></span>
							<?php 
								if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
									<a href="mailto:asc@editionventures.be">Aurélie Schoonjans</a>
									<!-- <a href="mailto:ema@editionventures.be">Emilie Mascia</a> -->
								<?php } else { ?>
									<a href="mailto:asc@editionventures.be">Aurélie Schoonjans</a>
									<a href="mailto:mko@editionventures.be">Mandy Kourkouliotis</a>
								<?php }; 
							?>
						</div>
						<div class="footer-div footer-right">
							<span><?php _e( "Suivez-nous!", "html5blank" ); ?></span>
							<span class="social">
								<a href="<?php echo get_field('socials_links','option')['facebook']; ?>" class="footer-social" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/img/facebook-b.png" alt="facebook"></a>
								<a href="<?php echo get_field('socials_links','option')['instagram']; ?>" class="footer-social" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/img/instagram-b.png" alt="instagram"></a>
								<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
								<a href="http://www.decoidees.be/newsletters/" class="footer-social"><img src="<?php bloginfo("template_directory"); ?>/img/mail-b.png" alt="mail"></a>
								<?php } else { ?>
								<a href="http://www.actiefwonen.be/newsletters/" class="footer-social"><img src="<?php bloginfo("template_directory"); ?>/img/mail-b.png" alt="mail"></a>
								<?php }; ?>
							</span>
							<a href="<?php _e( "http://www.decoidees.be/c-g-u/", "html5blank" ); ?>"><?php _e( "C.G.U.", "html5blank" ); ?></a>
							<a href="<?php _e( "http://www.decoidees.be/vie-privee/", "html5blank" ); ?>"><?php _e( "Vie privée", "html5blank" ); ?></a>
						</div>
					</div> 
				</div>
			</div>
			<!-- <div class="footer-bottom _background">
				<div class="container">
					<div class="row">
						 <span class="left">Copyright © <?php echo date("Y"); ?> <?php _e( "Déco Idées", "html5blank" ); ?></span>
						 <span class="right">
							<a href="<?php _e( "http://www.decoidees.be/c-g-u/", "html5blank" ); ?>"><?php _e( "C.G.U.", "html5blank" ); ?></a>
						</span>
					</div>
				</div>
			</div> -->
		</footer>
		<!-- Ventures -->
		<div class="footer-ventures _background">
			<div class="container-main">
				<a class="ventures-logo" href="http://www.editionventures.be/" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/img/logo_ventures.png" alt="logo Edition Ventures">
				</a>
				<div class="footer-right">
					<span><small>© <?php echo date("Y"); ?>&nbsp;</small>Edition Ventures</span>
					<ul class="sites_selector">
						<li><a href="https://www.elle.be/" target="_blank">ELLE</a></li>
						<li><a href="https://www.marieclaire.be/" target="_blank">Marie Claire</a></li>
						<li><a href="http://www.psychologies.com/" target="_blank">Psychologies</a></li>
						<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
							<li><a href="http://www.decoidees.be/" target="_blank">Déco Idées</a></li>
						<?php } else { ?>
							<li><a href="http://www.actiefwonen.be/" target="_blank">Actief Wonen</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<?php wp_footer(); ?>
        <div class="cookie-box box--cookie">
   			<div class="container">
   			<div class="cookie-closebtn"><span></span></div>
   			<?php _e( "En poursuivant votre navigation sur ce site, vous acceptez l'utilisation de cookies. Ces derniers assurent le bon fonctionnement de nos services.", "html5blank" ); ?> <a href="<?php _e( "http://www.decoidees.be/c-g-u/", "html5blank" ); ?>" class="_color" target="_blank"><?php _e( "En savoir plus sur les cookies", "html5blank" ); ?>.</a>
			</div>
   		</div>
 
		<script>
		console.log('LOADING INIT');
		window.onload = function(e){ 
			// make a stylesheet link
			/*
			console.log('DOCUMENT IS READY FOR LOADING');
			
			array_css =  [
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/css/style.min.css?ver=4.9.7',
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/css/jquery.fancybox.css',
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/css/plugins/swiper-2.7.6/idangerous.swiper.css',
			];

			array_css.forEach(function(element) {
				console.log(element);
				var myCSS = document.createElement( "link" );
				myCSS.rel = "stylesheet";
				myCSS.href = element;
				document.head.insertBefore( myCSS, document.head.childNodes[ document.head.childNodes.length - 1 ].nextSibling );
			});

			

			array_script =  [
				'//c.pebblemedia.be/js/c.js',
				'https://pool-pebblemedia.adhese.com/tag/tag.js',
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/js/lib/postscribe.js',
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/js/main.js',
				'http://localhost:8080/decoideesactiefwonen2018/wp-content/themes/diaw2018/app/js/swiper.min.js',
				'//assets.pinterest.com/js/pinit.js',
			];

			array_script.forEach(function(element) {
				console.log(element);
				var tag = document.createElement( "script" );
				tag.src = element;
				document.head.insertBefore( tag, document.head.childNodes[ document.head.childNodes.length - 1 ].nextSibling );
			});
			*/
		}

		</script>

		<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>	
			<script>
			var dm_eprivacyCookie_consent        		= true;
			var dm_gdpr_anonymousData_consent      		= true;
			var dm_gdpr_registrationData_consent        = true;
			var pbm_dl_user_skey 						= '';

			//do not change below this line
			var publisherCXcustomerPrefix          	= 'vnt';
			var _origin                            	= 'vnt-decoidees.be';
			var cX 									= cX || {};
			cX.callQueue 							= cX.callQueue || [];
			cX.callQueue.push(['requireConsent']);
			if( window.dm_gdpr_anonymousData_consent === true && typeof window.dm_gdpr_anonymousData_consent !== "undefined"){
				cX.callQueue.push(['setConsent', { pv: true, segment: true, ad: true, recs: true }]);
			}else{
				cX.callQueue.push(['setConsent', { pv: false, segment: false, ad: false, recs: false }]);
			}
			cX.callQueue.push(['setSiteId', '1139597790485793210']);

			</script>
			<script src="https://c.pebblemedia.be/js/data/pbm/PUB/decoidees.be.js" ></script>
			<script>
				(function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
				e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
				t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
			</script>
			<?php if(get_field('no_skimlink_script',get_the_ID()) == true){ } else{ ?>
				<script type="text/javascript" src="https://s.skimresources.com/js/131004X1593292.skimlinks.js"></script>
			<?php } ?>
		<?php } else { ?>
			<script>
			var dm_eprivacyCookie_consent        		= true;
			var dm_gdpr_anonymousData_consent      		= true;
			var dm_gdpr_registrationData_consent        = true;
			var pbm_dl_user_skey 						= '';

			//do not change below this line
			var publisherCXcustomerPrefix          	= 'vnt';
			var _origin                            	= 'vnt-actiefwonen.be';
			var cX 									= cX || {};
			cX.callQueue 							= cX.callQueue || [];
			cX.callQueue.push(['requireConsent']);
			if( window.dm_gdpr_anonymousData_consent === true && typeof window.dm_gdpr_anonymousData_consent !== "undefined"){
				cX.callQueue.push(['setConsent', { pv: true, segment: true, ad: true, recs: true }]);
			}else{
				cX.callQueue.push(['setConsent', { pv: false, segment: false, ad: false, recs: false }]);
			}
			cX.callQueue.push(['setSiteId', '1142975492203463067']);

			</script>
			<script src="https://c.pebblemedia.be/js/data/pbm/PUB/actiefwonen.be.js" ></script>
			<script>
				(function(d,s,e,t){e=d.createElement(s);e.type='text/java'+s;e.async='async';
				e.src='http'+('https:'===location.protocol?'s://s':'://')+'cdn.cxense.com/cx.js';
				t=d.getElementsByTagName(s)[0];t.parentNode.insertBefore(e,t);})(document,'script');
			</script>

			<?php if(get_field('no_skimlink_script',get_the_ID()) == true){ } else{ ?>
				<script type="text/javascript" src="https://s.skimresources.com/js/121690X1582675.skimlinks.js"></script>
			<?php } ?>
		<?php }; ?>
    </body>
</html>