<!-- single.php -->
<div class="container">
	<article>
		<div class="full-content">
			<h1 class="post-title"><?php the_title(); ?></h1>
			<p class="post-subtitle"><?php the_field('sous-titre'); ?></p>
			<div class="post-info">
				<time class="post-date" datetime="<?php 'Y-m-d' ?>"><?php the_time('d.m.Y') ?></time>
				<span class="tags _color">Tags</span>
				<!-- If no tags display the category -->
				<span class="post-tags"><?php _e( "Publi-reportage", "html5blank" ); ?>
				</span>
				<div class="share">
					<span>Partager</span>
					<!-- http://petragregorova.com/articles/social-share-buttons-with-custom-icons/ -->
					<a href="mailto:?subject=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>&amp;body=<?php print(urlencode(get_permalink())); ?>"><img src="<?php bloginfo("template_directory"); ?>/assets/img/a-mail.png"></a>
					<a href="http://www.facebook.com/sharer/sharer.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/assets/img/a-facebook.png"></a>
					<a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/assets/img/a-twitter.png"></a>
					<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php print(urlencode(the_post_thumbnail_url())); ?>&url=<?php print(urlencode(get_permalink())); ?>&is_video=false&description=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>" target="_blank"><img src="<?php bloginfo("template_directory"); ?>/assets/img/a-pinterest.png"></a>
				</div>
			</div>
			<div class="visuel">
				    <div class="post-cat _background">
				    	<?php
							$posttags = get_the_tags();
							if ($posttags) {
							  foreach($posttags as $tag) {
							    echo $tag->name . ' '; 
							  }
							}
						?>
				    </div>
			</div>
		</div>
		<div class="content">
			<?php the_post_thumbnail('d_single_n'); ?>
			<?php the_content(); ?>
			<div class="navigation"><ul>
			<?php
				wp_link_pages( array(
					'before'      => '<li>',
					'after'       => '</li>',
					'link_before' => '',
					'link_after'  => '',
					'pagelink'    => '<span class="_color">%</span>',
					'separator'   => '</li><li>',
				) );
			?>
			</ul></div>
			<div class="post-author _color"><?php _e( "par", "html5blank" ); ?> <span class="author"><?php the_author(); ?></span></div>
			<section class="related-posts">
				<h3 class="main-related-title _color"><?php _e( "Vous aimerez aussi", "html5blank" ); ?></h3>

				<?php $query = new WP_Query( array ( 'post_type' => 'publireportage', 'posts_per_page' => 3,  'order' => 'DESC')); ?>
				<?php  if ( $query->post_count >= 1)  { ?>
					<?php while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="row">
							<?php get_template_part( 'templates/template_publi' ); ?>
						</div>
						<?php wp_reset_postdata(); ?>
					<?php endwhile; ?>
				<?php } ?> 
			</section>
		</div>
		<?php get_sidebar(); ?>
	</article>
</div>