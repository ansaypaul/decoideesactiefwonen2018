<?php
/**
 * Plugin Name: DécoIdée FR - Advertising
 * Description: Advertising
 * Version: 1.0.0
 * Author: Ansay Paul
 * Author URI: mailto:pansay@adneom.com
 */

define("ADVERTISING_TYPE", false); // 'dfp' = DFP, other = Pebble

class Ad {
	protected $type;
	protected $configFile;
	protected $config;

	public function __construct($type = false) {
        $this->type = $type;
        $this->configFile = $type === 'dfp' ? 'dfp.json' : 'pebble.json';
        $this->setConfig();
        $this->setScripts();
        $this->injectScripts();
    }
     
    protected function setScripts() {
    	if($this->type === 'dfp') wp_enqueue_script( 'googletag', '//www.googletagservices.com/tag/js/gpt.js' );
	    wp_enqueue_script( 'postscribe', 'https://cdnjs.cloudflare.com/ajax/libs/postscribe/1.4.0/postscribe.min.js' );
	    wp_enqueue_script( 'pebble', 'https://c.pebblemedia.be/js/c.js' );
	    wp_enqueue_script( 'adhese', 'https://pool-pebblemedia.adhese.com/tag/tag.js' );
	    wp_enqueue_script( 'advertising', plugins_url( '/js/', __FILE__ ) . 'advertising.js' );
	    wp_localize_script( 'advertising', 'advertising', array('type' => $this->type, 'config' => $this->config));
    }

    protected function injectScripts() {
		add_action( 'wp_enqueue_scripts', $this->setScripts() );
    }

    protected function setConfig() {
    	$_config = file_get_contents( 'js/'. $this->configFile, true);
		$this->config = json_decode($_config, true);
    }

    public function getPosition() {
		$res = "others";
		if ( $_SERVER['REQUEST_URI'] === '/' ) {
			$res = "homepage";
		}
		return "'" . $res . "'";
    }
}

$ad = new Ad(ADVERTISING_TYPE);

?>