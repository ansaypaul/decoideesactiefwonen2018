#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Cookies legislation & GDPR - tarteaucitron.js\n"
"POT-Creation-Date: 2018-05-25 16:21+0200\n"
"PO-Revision-Date: 2018-05-25 16:21+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: tarteaucitron.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: Admin.php:41
msgid "Email or password incorrect"
msgstr ""

#: Admin.php:69
msgid "You need a valid account to use this plugin"
msgstr ""

#: Admin.php:69 Admin.php:84
msgid "Sign up"
msgstr ""

#: Admin.php:71
msgid "Login"
msgstr ""

#: Admin.php:75
msgid "Email"
msgstr ""

#: Admin.php:79
msgid "Password"
msgstr ""

#: Admin.php:97
msgid "Subscription active"
msgstr ""

#: Admin.php:99
msgid "Subscription expired (limited to 3 services)"
msgstr ""

#: Admin.php:110
msgid "Statistics"
msgstr ""

#: Admin.php:110
msgid "Logout"
msgstr ""

#: Admin.php:118
msgid ""
"Invisible services (analytics, APIs, ...) are on this page, the others "
"(social network, comment, ads ...) need to be added with <b><a href="
"\"widgets.php\">WordPress Widgets</a></b>"
msgstr ""

#: Sidebars.php:72
msgid "Before"
msgstr ""

#: Sidebars.php:73
msgid "For use before your content"
msgstr ""

#: Sidebars.php:75
msgid "After"
msgstr ""

#: Sidebars.php:76
msgid "For use after your content"
msgstr ""

#: Sidebars.php:80
msgid "all content type except posts"
msgstr ""

#: Sidebars.php:82
msgid "posts"
msgstr ""

#: Sidebars.php:93
msgid "Unassigned"
msgstr ""

#: Sidebars.php:94
msgid "For use with page and theme builder"
msgstr ""

#: Widgets.php:13
msgid "Add services"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Cookies legislation & GDPR - tarteaucitron.js"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://opt-out.ferank.eu/en/"
msgstr ""

#. Description of the plugin/theme
msgid "Comply with the Cookies and GDPR legislation."
msgstr ""

#. Author of the plugin/theme
msgid "AIC Agency SAS"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://aic.agency/"
msgstr ""
