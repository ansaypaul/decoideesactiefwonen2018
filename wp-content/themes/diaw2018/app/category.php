<?php get_header(); ?>
	<main class="main">
		<div class="container-main">
			<div class="full-content">
				<h1 class="category">
					<?php single_cat_title(); ?>
				</h1>
				<div class="sub-category">
				    <?php
					    $parentCatName = single_cat_title('',false);
					    $parentCatID = get_cat_ID($parentCatName);
					    $current_category = get_the_category($parentCatID);
					    $childCats = get_categories( 'child_of='.$parentCatID );
					    if(is_array($childCats)):
					    foreach($childCats as $child){ 
						$category_link = get_category_link( $child->term_id ); ?>
					    <span><a class="_color" href="<?php echo esc_url( $category_link ); ?>"><?php echo $child->name; ?></a></span>
					    <?php query_posts('cat='.$child->term_id);
					    wp_reset_query();
					    }
					    endif;
					?> 
				</div>
				<div class="desc-category"><?php echo category_description(); ?></div>
			</div>
			<div class="main-content">
				<?php $latest_cat_post = new WP_Query( array('post_type' => 'post','posts_per_page' => 10,'paged'=>$paged,'category__in' => get_query_var('cat')));			
					if( $latest_cat_post->have_posts() ) :
						$i = 0; 
						while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
								$i++;
								if($i == 2 || $i == 5 || $i == 8){ ?>
								<div class="row row-imu">
									<div id="imu-<?php display_id_random()?>" class="ads" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="others" data-position="2"></div>
								</div>
								<?php }
								get_template_part( 'templates/template_category' );
						endwhile;
					endif;

				?>
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</main>
<?php get_footer(); ?>