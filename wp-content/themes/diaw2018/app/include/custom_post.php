<?php
/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

//Add custom type Cover
register_post_type(
  'Cover',
  array(
    'label' => 'Cover',
    'labels' => array(
      'name' => 'Cover',
      'singular_name' => 'Cover',
      'all_items' => __('Toutes les covers', 'html5blank'),
      'add_new_item' => __('Ajouter une cover', 'html5blank'),
      'edit_item' => __('Éditer la cover', 'html5blank'),
      'new_item' => __('Nouvelle cover', 'html5blank'),
      'view_item' => __('Voir la cover', 'html5blank'),
      'search_items' => __('Rechercher parmi les covers', 'html5blank'),
      'not_found' => __('Pas de cover trouvée', 'html5blank'),
      'not_found_in_trash'=> __('Pas de cover dans la corbeille', 'html5blank')
      ),
    'public' => true,
    'capability_type' => 'post',
    'menu_icon'   => 'dashicons-format-image',
    'menu_position' => 4,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail'
    ),
    'has_archive' => true
  )
);
