<!-- Fullpost -->
<article class="post-second">
	<a href="<?php the_permalink(); ?>">
		<figure>
			<?php the_post_thumbnail('d_full'); ?>
			<figcaption>
				<?php foreach((get_the_category()) as $category) {  ?>
				<?php 
					if($category->slug == 'publireportage')
					{
						$tag_name = wp_get_post_tags(get_the_ID());
						echo '<span class="publi_tag _background">'.$tag_name[0]->name.'</span>'; 
					} 
				?>
				<?php if (!empty( $category ) && (!is_category()) && ($category->slug != 'slider') && ($category->slug != 'publireportage')  && $category->parent == 0 ) { ?>
						<span class="post-category _background">
							<?php echo esc_html( $category->cat_name); ?>
						</span>
					<?php 
					break;
					}	?>
				<?php } ?>
				<h2 class="post-title"><?php the_title(); ?></h2>
			</figcaption>
			<div class="_background"></div>
		</figure>
	</a>
</article>