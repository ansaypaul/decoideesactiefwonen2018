<!-- page.php -->
<section class="main-wrapper">
		<main class="main">
			<div class="container-main">
				<div class="full-content">
					<h1 class="page-title"><?php the_title(); ?></h1>
				</div>
				<div class="main-content">
					<?php the_content(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</main>
	</section>
</article>