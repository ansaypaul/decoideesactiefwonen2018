<?php
/**
 * Plugin Name: Linkinbio
 * Description: Tu n'as encore rien vu jeune paupiette! 
 * Version: 1.0.0
 * Author: Elodie Buski & Paul Ansay
 * Author URI: mailto:elodie@editionventures.com mailto:paul@editionventures.com
 */

// Unregister style sheet.


if (function_exists('add_theme_support'))
{
    add_theme_support('post-thumbnails');
    /* CURRENT IMAGE */
    add_image_size( 'instagram_linkinbio', 300, 300, true );
}

function remove_unnecessary_stylesheet() {
    wp_dequeue_style( 'html5blank_styles' );
    wp_deregister_style( 'html5blank' );
    wp_dequeue_style( 'swiper' );
    wp_deregister_style( 'swiper' );
    wp_dequeue_style( 'customscrollbar' );
    wp_deregister_style( 'customscrollbar' );
}

add_action( 'template_redirect', 'wpse_128636_redirect_post' );

function my_remove_wp_seo_meta_box() {
	remove_meta_box('wpseo_meta', 'linkinbio', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);


function wpse_128636_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  if ( is_single() && 'linkinbio' ==  $queried_post_type ) {
    wp_redirect( home_url().'/linkinbio', 302 );
    exit;
  }
}

// Register style sheet.
//add_action( 'wp_enqueue_scripts', 'register_plugin_styles' );

function LinkinbioCSS() {
	wp_register_style( 'LinkinbioCSS', plugins_url( '/css/linkinbio.min.css', __FILE__ ), array(), '1.0.0', 'all');
	wp_enqueue_style( 'LinkinbioCSS' );
}
add_action( 'wp_enqueue_scripts', 'LinkinbioCSS' );

function LinkinbioJS(){
	wp_register_script( 'LinkinbioJS',  plugins_url( '/js/linkinbio.js', __FILE__ ), array( 'jquery' ));
	wp_enqueue_script( 'LinkinbioJS');
	wp_localize_script('LinkinbioJS', 'myLinkinbioJS', array(
    'pluginsUrl' => plugins_url(),
	));
}
add_action( 'admin_enqueue_scripts', 'LinkinbioJS' );

// ACF PRO CALL
//1. customize ACF path
add_filter('acf/settings/path', 'my_acf_settings_path');
 
function my_acf_settings_path( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/acf/';
    // return
    return $path;
}

// // 2. customize ACF dir
add_filter('advanced-custom-fields-pro/settings/dir', 'my_acf_settings_dir');
 
function my_acf_settings_dir( $dir ) {
    // update path
    $dir = get_stylesheet_directory_uri() . '/advanced-custom-fields-pro/';
    // return
    return $dir;
}

// // 3. Hide ACF field group menu item
add_filter('advanced-custom-fields-pro/settings/show_admin', '__return_false');

// // 4. Include ACF
//include_once( get_stylesheet_directory() . '/advanced-custom-fields-pro/acf.php' );


//ACF Field call
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5b056b17391f9',
	'title' => 'Link in bio (settings)',
	'fields' => array (
		array (
			'key' => 'field_5b056b276aa43',
			'label' => 'Intro phrase',
			'name' => 'linkinbio_intro_phrase',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array (
			'key' => 'field_5b056b646aa44',
			'label' => 'Website image',
			'name' => 'linkinbio_website_image',
			'type' => 'image',
			'instructions' => 'PNG height 90px',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):
acf_add_local_field_group(array (
	'key' => 'group_5afaabb03464f',
	'title' => 'Links in bio',
	'fields' => array (
		array (
			'key' => 'field_instagram_link',
			'label' => 'Instagram link',
			'name' => 'instagram_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array (
			'key' => 'field_5afaabe17cfbe',
			'label' => 'Instagram pic link',
			'name' => 'instagram_pic_link',
			'type' => 'image',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'instagram',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_5afaac1c7cfbf',
			'label' => 'Relative link',
			'name' => 'relative_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'linkinbio',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'the_content',
		1 => 'excerpt',
		2 => 'discussion',
		3 => 'comments',
		4 => 'author',
		5 => 'format',
		6 => 'page_attributes',
		7 => 'categories',
		8 => 'tags',
		9 => 'send-trackbacks',
	),
	'active' => 1,
	'description' => '',
));

endif;

//Register post type Linkinbio
function register_custom_post_type()
{
    register_post_type(
	  'linkinbio',
	  array(
	    'label' => 'links in bio',
	    'labels' => array(
	      'name' => 'Links in bio',
	      'singular_name' => 'link in bio',
	      'all_items' => __('Tous les links in bio', 'html5blank'),
	      'add_new_item' => __('Ajouter un link in bio', 'html5blank'),
	      'edit_item' => __("Éditer le link in bio", 'html5blank'),
	      'new_item' => __('Nouveau link in bio', 'html5blank'),
	      'view_item' => __("Voir le link in bio", 'html5blank'),
	      'search_items' => __('Rechercher parmi les links in bio', 'html5blank'),
	      'not_found' => __("Pas de links in bio trouvés", 'html5blank'),
	      'not_found_in_trash'=> __("Pas de link in bio dans la corbeille", 'html5blank')
	      ),
	    'public' => true,
	    'capability_type' => 'post',
	    'menu_icon'   => 'dashicons-admin-links',
	    'menu_position' => 6,
	    'supports' => array(
	      'title',
	      'author',
	      'editor',
	      'thumbnail'
	    ),
	    'has_archive' => true,
	  )
	);
}
add_action('init', 'register_custom_post_type');

//Including Single- & Archive- templates
add_filter('single_template','linkinbio_single');
add_filter('archive_template','linkinbio_archive');

//Single- template
function linkinbio_single($single_template){
  global $post;
  $found = locate_template('single-linkinbio.php');
  if($post->post_type == 'linkinbio' && $found == ''){
    $single_template = dirname(__FILE__).'/templates/single-linkinbio.php';
  }
  return $single_template;
}

//Archive- template
function linkinbio_archive($template){
  if(is_post_type_archive('linkinbio')){
    $theme_files = array('archive-linkinbio.php');
    $exists_in_theme = locate_template($theme_files, false);
    if($exists_in_theme == ''){
      return plugin_dir_path(__FILE__) . '/templates/archive-linkinbio.php';
    }
  }
  return $template;
}

function Generate_Featured_Image_linkinbio( $image_url, $post_id  ){
    $upload_dir = wp_upload_dir();
    $image_data = file_get_contents($image_url);
    $filename = basename($image_url);
    if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    file_put_contents($file, $image_data);

    $wp_filetype = wp_check_filetype($filename, null );
    $filename_sanitize = sanitize_file_name($filename);
    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title' => sanitize_file_name($filename),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $check_current_media = post_exists($filename);
    if($check_current_media == false){
        $attach_id = wp_insert_attachment( $attachment, $file );
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
        //$res2= set_post_thumbnail( $post_id, $attach_id );
        return $attach_id;
    }else{
        return $check_current_media;
    }
}

/* INSTAGRAM ADMIN AJAX */
add_action( 'wp_ajax_nopriv_erase_uploaded_images_instagram', 'erase_uploaded_images_instagram' );
add_action( 'wp_ajax_erase_uploaded_images_instagram', 'erase_uploaded_images_instagram' );

function erase_uploaded_images_instagram(){

    $url_insta = "https://api.instagram.com/oembed/?url=".$_POST['url_instagram'];
    $json = file_get_contents($url_insta);
    $json_data = json_decode($json,true);
    $insta_author_name = $json_data['author_name'];
    $insta_title = $json_data['title'];
    $insta_thumb = $json_data['thumbnail_url'];
    $insta_thumb_img_id = Generate_Featured_Image_linkinbio($json_data['thumbnail_url']);
    $json_data['insta_thumb_img_id'] = $insta_thumb_img_id;
    echo json_encode($json_data);
    die();
}