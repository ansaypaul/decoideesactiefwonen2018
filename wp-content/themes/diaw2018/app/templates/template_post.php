<!-- Post -->
<article class="post-second">
	<a href="<?php the_permalink(); ?>">
		<figure>
			<?php the_post_thumbnail('d_news'); ?>
			<figcaption>
				<?php foreach((get_the_category()) as $category) { ?>
					<?php if (!empty( $category ) && (!is_category()) && ($category != 'slider')  && $category->parent == 0 ) { ?>
						<span class="post-category _background">
							<?php 
							if($category->slug == 'publireportage')
							{
								$tag_name = wp_get_post_tags(get_the_ID());
								echo esc_html( $tag_name[0]->name); 
							} 
							else
							{
								echo esc_html( $category->cat_name);
							}
							?>
						</span>
					<?php 
					break;
					}	?>
				<?php } ?>
				<h2 class="post-title"><?php the_title(); ?></h2>
				<?php the_excerpt(); ?>
			</figcaption>
		</figure>
	</a>
</article>