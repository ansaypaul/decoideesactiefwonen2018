<?php
/**
 * Plugin Name: Stuctured data schema
 * Description: Que le web soit avec toi!
 * Version: 1.0.0
 * Author: Elodie Buski & Paul Ansay
 * Author URI: mailto:elodie@editionventures.com mailto:paul@editionventures.com
 */

function awesome_excerpt($text, $raw_excerpt) {
    if( ! $raw_excerpt ) {
        $content = apply_filters( 'the_content', get_the_content() );
        $text = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
    }    
    return $text;
}

function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}

function js_array($array)
{
    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}


function generate_structured_data(){
	$site_url = get_bloginfo('url');
	$site_name = get_bloginfo('name');
	$article_title = html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8');
	$article_thumb = get_the_post_thumbnail_url();
	$article_excerpt = html_entity_decode(strip_tags(awesome_excerpt()));
	$content = strip_tags(get_the_content());
	$article_id = get_the_ID();
	$permalink = get_the_permalink();
	$post_tags = get_the_tags();$list_tags_str = '';
	$word_count = str_word_count( strip_tags( get_post_field( 'post_content', $article_id ) ) );

    foreach( $post_tags as $tag ) {
    	$list_tag = ucfirst($tag->name);
    	$article_tags .= $list_tag.', '; 
    } 
    $list_tags_str = substr($list_tags_str, 0, -2).'.';
	$date_modified = get_the_modified_time('Y-m-d\TH:i:sO');
	$date_published = get_the_time('Y-m-d\TH:i:sO');
	$post_author_id = get_post_field( 'post_author', $article_id  );
	$author_name = get_the_author_meta('display_name', $post_author_id);
	$author_url = esc_url( get_author_posts_url( $post_author_id ) );
	$author_jobtitle = get_the_author_meta( 'authortitle' );
	$recipe_ID =  get_field('st_id','option')['sd_recette_id'];
	$recipepreptime = explode(':',get_field( "preparation" )); 
	$recipepreptime_structure = "00";
		if(count($recipepreptime) > 1){
			$recipepreptime_heure = intval($recipepreptime[0]);
			$recipepreptime_min = intval($recipepreptime[1]);
			$recipepreptime_s = intval($recipepreptime[2]);
			$recipepreptime_structure = $recipepreptime_heure * 60 + $recipepreptime_min + ($recipepreptime_s / 60);
		}
	$recipecooktime = explode(':',get_field( "cuisson" )); 
	$recipecooktime_structure = "00";
	if(count($recipecooktime) > 1){
		$recipecooktime_heure = intval($recipecooktime[0]);
		$recipecooktime_min = intval($recipecooktime[1]);
		$recipecooktime_s  = intval($recipecooktime[2]);
		$recipecooktime_structure = $recipecooktime_heure * 60 + $recipecooktime_min + ($recipecooktime_s / 60);
				}
	$recipetotaltime = $recipepreptime_structure+$recipecooktime_structure;
	$recipeintro = strip_tags(get_field( "introduction" ));
	$recipeyield = get_field( "quantite" ); 
	$recipedifficulties = get_field( "difficulte" );
	$recipetypes = get_field( "recipe-type");
		foreach( $recipetypes as $recipetype ): 
			$list_category = ucfirst($recipetype);
			$recipe_category .= $list_category.', ';
		endforeach;

	$recipeingredients = get_field( "ingredients");
	$custom_logo_id = get_theme_mod( 'custom_logo' );
	$image_logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	
	if (in_category($recipe_ID)) {
		$recipeingredients = str_replace('<ul>','',$recipeingredients);
		$recipeingredients = str_replace('</ul>','',$recipeingredients);
		$recipeingredients_array = explode('</li>',$recipeingredients);
		foreach ($recipeingredients_array as $recipe) {
			$recipe =  strip_tags($recipe);
			$recipeingredients_array_format[] = $recipe;
		}


		$recipesteps = get_the_content();
		$recipesteps = get_post_field( 'post_content', $article_id );
		$recipesteps = str_replace('<ol>','',$recipesteps);
		$recipesteps = explode('</ol>',$recipesteps)[0];
		$recipesteps_array = explode('</li>',$recipesteps);
		foreach ($recipesteps_array as $recipe) {
			$recipe =  strip_tags($recipe);
			$recipesteps_array_format[] = $recipe;
		}

	?>

		<script type="application/ld+json">
		{
		  "@context": "http://schema.org/",
	      "@type": "Recipe",
	      "name": "<?php echo $article_title; ?>",
	      "image": [
		    "<?php echo $article_thumb; ?>"
		  ],
	      "author": {
		    "@type": "Person",
		    "name": "<?php echo $author_name; ?>",
		    "url": "<?php echo $author_url; ?>",
		    "jobTitle": "<?php echo $author_jobtitle; ?>"
		  },
	      "datePublished": "<?php echo $date_modified; ?>",
	      "dateModified": "<?php echo $date_published; ?>",
	      "description": "<?php echo $recipeintro; ?>",
	      "keywords": "<?php echo substr($article_tags, 0, -2); ?>",
	      "recipeYield": "<?php echo $recipeyield; ?>",
	      "recipeCategory": "<?php echo substr($recipe_category, 0, -2); ?>",
	      "recipeIngredient": <?php echo js_array($recipeingredients_array_format); ?>,
	      "recipeInstructions": <?php echo js_array($recipesteps_array_format); ?>,
	        "publisher": {
			    "@type": "Organization",
			    "name": "<?php echo $site_name; ?>",
			    "logo": {
			      "@type": "ImageObject",
			      "url": "<?php echo get_field('sd_site_logo','option' ); ?>"
			    }
			}
		}
		</script>
	<?php 

	} else if(is_single()){ ?>
			<script type="application/ld+json">
			{
			  "@context": "http://schema.org",
			  "@type": "Article",
			  "url": "<?php echo $permalink; ?>",
			  "mainEntityOfPage": {
			    "@type": "WebPage",
			    "@id": "<?php echo $permalink; ?>"
			  },
			  "headline": <?php echo json_encode($article_title); ?>,
			  "image": {
				  "@type": "ImageObject",
				  "url": "<?php echo $article_thumb; ?>"
				},
			  "description": <?php echo json_encode($article_excerpt); ?>,
			  "articleBody": <?php echo json_encode($content); ?>,
			  "datePublished": "<?php echo $date_modified; ?>",
			  "dateModified": "<?php echo $date_published; ?>",
			  "author": {
			    "@type": "Person",
			    "name": "<?php echo $author_name; ?>",
			    "url": "<?php echo $author_url; ?>"
			  },
			   "publisher": {
			    "@type": "Organization",
			    "name": "<?php echo $site_name; ?>",
			    "logo": {
			      "@type": "ImageObject",
			      "url": "<?php echo get_field('sd_site_logo','option' ); ?>"
			    }
			  }
			}
			</script>
		<?php } else {
	}
};

add_action( 'wp_head', 'generate_structured_data', 10);

// ACF-OPTIONS
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5b1122c60fb9c',
	'title' => 'Structured data schema',
	'fields' => array (
		array (
			'key' => 'field_5b1122d87fe58',
			'label' => 'Site Logo',
			'name' => 'sd_site_logo',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => 200,
			'min_height' => 200,
			'min_size' => '',
			'max_width' => 200,
			'max_height' => 200,
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_5b112914de062',
			'label' => 'Stuctured_Data_ID',
			'name' => 'st_id',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layout' => 'table',
			'sub_fields' => array (
				array (
					'key' => 'field_5b112a3df1fae',
					'label' => 'Fiche DIY ID',
					'name' => 'sd_diy_id',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				array (
					'key' => 'field_5b11296dde063',
					'label' => 'Fiche recette ID',
					'name' => 'sd_recette_id',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5a3cddd417a23',
	'title' => 'DIY',
	'fields' => array(
		array(
			'key' => 'field_5860db5f2ead7',
			'label' => 'Introduction',
			'name' => 'introduction',
			'type' => 'wysiwyg',
			'instructions' => 'Petite introduction du DIY',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
		array(
			'key' => 'field_5860db942ead8',
			'label' => 'Matériel',
			'name' => 'materiel',
			'type' => 'wysiwyg',
			'instructions' => 'Remplir le matériel dans une liste à puces',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
		array(
			'key' => 'field_5860dc762ead9',
			'label' => 'Difficulté',
			'name' => 'difficulte',
			'type' => 'text',
			'instructions' => 'Indiquer le niveau de difficulté: Très facile/Facile/Moyen/Difficile',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Facile',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5860dca82eada',
			'label' => 'Temps réalisation',
			'name' => 'realisation',
			'type' => 'text',
			'instructions' => 'Indiquer le temps de réalisation',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Exemple: 20min',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5860dcd02eadb',
			'label' => 'Source',
			'name' => 'source',
			'type' => 'wysiwyg',
			'instructions' => 'Remplir si nécessaire',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
			array(
				'param' => 'post_category',
				'operator' => '==',
				'value' => '3',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
	),
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5a3cddd429bb9',
	'title' => 'Recette',
	'fields' => array(
		array(
			'key' => 'field_5851240b51b3a',
			'label' => 'Introduction',
			'name' => 'introduction',
			'type' => 'wysiwyg',
			'instructions' => 'Petite introduction de la recette',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
		array(
			'key' => 'field_5851202a4776d',
			'label' => 'Ingrédients',
			'name' => 'ingredients',
			'type' => 'wysiwyg',
			'instructions' => 'Remplir les ingrédients dans une liste à puces',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
		array(
			'key' => 'field_58512696a2e75',
			'label' => 'Quantité',
			'name' => 'quantite',
			'type' => 'text',
			'instructions' => 'Remplir la "quantité" de cette recette',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Exemples: 4 personnes ou 8 tartelettes',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5851271061b9c',
			'label' => 'Difficulté',
			'name' => 'difficulte',
			'type' => 'text',
			'instructions' => 'Indiquer le niveau de difficulté: Très facile/Facile/Moyen/Difficile',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Facile',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_585138645f3e6',
			'label' => 'Temps préparation',
			'name' => 'preparation',
			'type' => 'text',
			'instructions' => 'Indiquer le temps de préparation',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Exemple: 20min',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5851397b174db',
			'label' => 'Temps de cuisson',
			'name' => 'cuisson',
			'type' => 'text',
			'instructions' => 'Remplir temps de cuisson (si applicable)',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'formatting' => 'html',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5851665ca2cb6',
			'label' => 'Source',
			'name' => 'source',
			'type' => 'wysiwyg',
			'instructions' => 'Remplir si nécessaire',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'toolbar' => 'full',
			'media_upload' => 1,
			'tabs' => 'all',
			'delay' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
			array(
				'param' => 'post_category',
				'operator' => '==',
				'value' => '23',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
	),
	'active' => 1,
	'description' => '',
));

endif;