��          �   %   �      P     Q     `     m     s  -   z  -   �     �     �     �       #   /  �   S                      
   $     /  ,   C  
   p  +   {     �     �     �     �  �  �     �     �     �     �  ;   �  6   !     X     n  !   �      �  *   �  �   �  	   �	     �	     �	     �	     
     
  *   %
     P
  A   ]
  &   �
     �
     �
     �
                                                        	                                          
                           AIC Agency SAS Add services After Before Comply with the Cookies and GDPR legislation. Cookies legislation & GDPR - tarteaucitron.js Email Email or password incorrect For use after your content For use before your content For use with page and theme builder Invisible services (analytics, APIs, ...) are on this page, the others (social network, comment, ads ...) need to be added with <b><a href="widgets.php">WordPress Widgets</a></b> Login Logout Password Sign up Statistics Subscription active Subscription expired (limited to 3 services) Unassigned You need a valid account to use this plugin all content type except posts https://aic.agency/ https://opt-out.ferank.eu/en/ posts Project-Id-Version: Cookies legislation & GDPR - tarteaucitron.js
POT-Creation-Date: 2018-05-25 16:20+0200
PO-Revision-Date: 2018-05-25 16:21+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-Basepath: ..
X-Poedit-WPHeader: tarteaucitron.php
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 AIC Agency SAS Ajouter des services Après Avant Se conformer à la législation sur les cookies et le RGPD. Législation sur les cookies & RGPD - tarteaucitron.js Adresse de messagerie Email ou mot de passe incorrect Pour utiliser après vos contenus Pour utiliser avant vos contenus Pour utiliser avec un constructeur de page Les services invisibles (mesure d'audience, APIs, …) sont présents sur cette page, les autres (réseaux sociaux, régies publicitaires, …) sont à ajouter via <b><a href="widgets.php">les Widgets WordPress</a></b> Connexion Déconnexion Mot de passe Inscription Statistiques Abonnement actif Abonnement expiré (limité à 3 services) Non affecté Vous avez besoin d'un compte valide pour utiliser cette extension tout type de contenu sauf les articles http://aic.agency/ https://opt-out.ferank.eu/fr/ articles 