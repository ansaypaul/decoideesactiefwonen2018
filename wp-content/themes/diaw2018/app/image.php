<?php get_header(); ?>
	<main class="main">
		<?php
			/* Most popular*/
			$now   = time();
			$date_post = strtotime(get_the_date('Y-m-d'));
			$diff  = abs($now - $date_post);
			wpb_set_post_views(get_the_ID());
		?>
		<?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>    
   				<?php get_template_part( 'templates/template_image' ); ?>
            <?php endwhile; ?>
        <?php endif; ?>
	</main>
<?php get_footer(); ?>