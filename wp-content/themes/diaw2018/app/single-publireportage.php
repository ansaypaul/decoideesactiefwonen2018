<?php get_header(); ?>
	<main class="main">
		<?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>    
               <?php get_template_part( 'templates/template_single-publireportage' ); ?>
            <?php endwhile; ?>
        <?php endif; ?>
	</main>
<?php get_footer(); ?>