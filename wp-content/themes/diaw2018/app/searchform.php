<div class="search-wrapper">
	<div class="search-closebtn"><span></span></div>
	<form role="search" method="get" id="search-form" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input class="search-input" placeholder="Rechercher..." type="text" value="<?php echo get_search_query(); ?>">
		<input class="search-btn" value="<?php _e( "Rechercher", "html5blank" ); ?>" type="submit" value="<?php echo esc_attr_x( '<?php _e( "recherche", "html5blank" ); ?>', 'submit button' ); ?>">
	</form>
</div>