<?php get_header(); ?>
	<main class="main">
		<div class="container-main">
			<div class="full-content">
				<h1 class="category">
					<?php _e( "Votre recherche", "html5blank" ); ?>: <?php echo $_GET['s']; ?>
				</h1>
				<div class="sub-category">
				</div>
			</div>
			<div class="main-content">
				<?php $latest_cat_post = new WP_Query( array('post_type' => 'post','posts_per_page' => 10,'s' => $_GET['s'],'paged'=>$paged,'category__in' => get_query_var('cat')));		
					if( $latest_cat_post->have_posts() ) :
						$i = 0; 
						while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
								$i++;
								if($i == 2 || $i == 5 || $i == 8){ ?>
								<div class="row row-imu">
									<div id="imu-<?php display_id_random()?>" class="ads" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="others" data-position="2"></div>
								</div>
								<?php }
								get_template_part( 'templates/template_category' );
						endwhile;
					endif;

				?>
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</main>
<?php get_footer(); ?>