<?php /* Template Name: Gallery Template */ ?>

<?php get_header(); ?>
<main class="main">
<?php 
				$ids_gal = explode( ',', $_POST['gallery_ids'] );
				$parent_post = get_post($post->post_parent);  
				$title = $_POST['gallery_title'];
				$parent_id = $_POST['postparent_id'];
				$parent_link = get_permalink($_POST['postparent_id']);
				$categories = get_the_category($post->post_parent);
				$categorie = $categories[0];
				$category_link = get_category_link( $categorie->cat_ID );
				$current_gallery_id = $_POST['current_gallery_id'];


?>

<div id="container" class="container-main">
	<article>
		<div class="full-content">
			<h1 class="post-title"><?php echo $title ?></h1>
		</div>
		<div class="content">
		<?php 	

			if ( count( $ids_gal ) > 1 ) {
				$number_current = $ids_gal[$current_gallery_id];
				$prev = $current_gallery_id - 1;
				$next = $current_gallery_id + 1; 
				if(count( $ids_gal ) == $current_gallery_id){
				$current_gallery_id = 0;
				$number_current = $ids_gal[$current_gallery_id];
				}
				if(0 ==  $current_gallery_id){
				$prev = count( $ids_gal ) - 1;
				
				}
			}

			$media_gal =  get_post($number_current);
		?>
			<div class="img-gallery-container">
				<a href="<?php echo $parent_link; ?>" class="btn-back _background"><?php _e( "Retour", "html5blank" ); ?></a>
				<span class="img-counter _background"><?php echo $current_gallery_id+1 ?>/<?php echo count($ids_gal) ?></span>
				<form id="my_next" action="" method="post">
					<input type="hidden" value="<?php echo $_POST['gallery_ids']; ?>" name="gallery_ids"/>
					<input type="hidden" value="<?php echo $title; ?>" name="gallery_title"/>
					<input type="hidden" value="<?php echo $parent_id; ?>" name="postparent_id"/>
					<input type="hidden" value="<?php echo $next; ?>" name="current_gallery_id"/>
				</form> 

				<form id="my_prev" action="" method="post">
					<input type="hidden" value="<?php echo $_POST['gallery_ids']; ?>" name="gallery_ids"/>
					<input type="hidden" value="<?php echo $title; ?>" name="gallery_title"/>
					<input type="hidden" value="<?php echo $parent_id; ?>" name="postparent_id"/>
					<input type="hidden" value="<?php echo $prev; ?>" name="current_gallery_id"/>
				</form> 
				<a class="previous image-control" onclick="document.getElementById('my_prev').submit(); return false;" href="#">
					<span class="_background control-top"></span>
					<span class="_background control-bottom"></span>
				</a>
				<a class="next image-control" onclick="document.getElementById('my_next').submit(); return false;" href="#">
					<span class="_background control-top"></span>
					<span class="_background control-bottom"></span>
				</a>
				<?php echo wp_get_attachment_image( $number_current, 'd_gallery' ); ?>
			</div>
			<div class="img-legend">
				<div class="titre"><?php  echo $media_gal->post_excerpt;  ?></div>
				<div class="prix"><a target="_blank" href="<?php  echo $media_gal->post_content;  ?>"><?php echo get_post_meta($number_current,'_wp_attachment_image_alt', true); ?></a></div></div>
			</div>

			<?php


      				 // var_dump($media_gal);
      				

		    ?>

			<!-- <div class="post-author _color">par <span><?php the_author(); ?></span></div> -->
		<?php get_sidebar(); ?>
	</article>
</div>
	</main>
<?php get_footer(); ?>