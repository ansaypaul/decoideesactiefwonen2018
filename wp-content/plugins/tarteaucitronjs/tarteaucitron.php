<?php
/*
Plugin Name: Cookies legislation & GDPR - tarteaucitron.js
Plugin URI: https://opt-out.ferank.eu/en/
Description: Comply with the Cookies and GDPR legislation.
Version: 1.2.1
Text Domain: tarteaucitronjs
Domain Path: /languages/
Author: AIC AGENCY SAS
Author URI: https://aic.agency/
Licence: GPLv2
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

define( 'TARTEAUCITRON_FILE'            	, __FILE__ );
define( 'TARTEAUCITRON_PATH'       		, realpath( plugin_dir_path( TARTEAUCITRON_FILE ) ) . '/' );


add_action( 'plugins_loaded', 'tarteaucitron_load_textdomain' );
function tarteaucitron_load_textdomain() {
    load_plugin_textdomain( 'tarteaucitronjs', false, TARTEAUCITRON_PATH . '/languages' ); 
}

require(TARTEAUCITRON_PATH . '/Admin.php');
require(TARTEAUCITRON_PATH . '/Sidebars.php');
require(TARTEAUCITRON_PATH . '/Widgets.php');

function tarteaucitron_post($query, $needLogin = 1) {
    $query .= '&langWP='.substr(get_locale(), 0, 2);
    if ($needLogin == 1) {
        $query .= '&uuid='.get_option('tarteaucitronUUID').'&token='.get_option('tarteaucitronToken').'&website='.$_SERVER['SERVER_NAME'];
    }

	parse_str($query, $query_array);

	$response = wp_remote_post( 'https://opt-out.ferank.eu/pro/wordpress/token.php', array(
		'method' => 'POST',
		'timeout' => 45,
		'redirection' => 5,
		'body' => $query_array,
		'blocking' => true,
		'sslverify' => false,
    	)
	);

	//print_r($query_array);

	if ( !is_wp_error( $response ) ) {
		//print_r($response['body']);
		return $response['body'];
	}

	return "0";
}

add_action('wp_enqueue_scripts', 'tarteaucitron_user_css_js');
function tarteaucitron_user_css_js() {
	$domain = $_SERVER['SERVER_NAME'];
	wp_register_style('tarteaucitronjs', plugins_url('tarteaucitronjs/css/user.css'));

    wp_enqueue_style('tarteaucitronjs');


    wp_enqueue_script('tarteaucitron_pro', 'https://opt-out.ferank.eu/tarteaucitron.pro.js?iswordpress=true&domain='.$domain.'&uuid='.get_option('tarteaucitronUUID'), array(), false, true);
}

add_action( 'admin_bar_menu', 'tarteaucitron_toolbar', PHP_INT_MAX );
function tarteaucitron_toolbar( $wp_admin_bar ) {
	$wp_admin_bar->add_menu( array(
		'id'    => 'tarteaucitronjs',
		'title' => '<span class="ab-icon"></span> tarteaucitron.js',
		'href'  => admin_url('options-general.php?page=tarteaucitronjs'),
	) );
}

add_action( 'admin_print_styles', '_tarteaucitron_admin_bar_css', 100 );
add_action( 'wp_print_styles', '_tarteaucitron_admin_bar_css', 100 );
function _tarteaucitron_admin_bar_css() {

	if (current_user_can( 'manage_options' )) {
		wp_register_style(
			'tarteaucitronjs-admin-bar',
			plugins_url('tarteaucitronjs/css/admin-bar.min.css'),
			array(),
			'1'
		);

		wp_enqueue_style( 'tarteaucitronjs-admin-bar' );
	}
}