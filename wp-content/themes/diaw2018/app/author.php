<?php get_header(); ?>
	<main class="main">
		<div class="container-main">
			<div class="full-content">
				<?php 
					$user_email = get_the_author_meta( 'user_email' );
					$user_title = get_the_author_meta( 'authortitle' ); 
					$user_descr = get_the_author_meta( 'description' );
					$user_instagram = get_the_author_meta( 'instagram' ); 
					$user_pinterest = get_the_author_meta( 'pinterest' ); 
					$fname = get_the_author_meta('first_name');
					$lname = get_the_author_meta('last_name');
					$ID = get_the_author_meta( 'ID' );
					$full_name = '';
					if( empty($fname)){
					    $full_name = $lname;
					} elseif( empty( $lname )){
					    $full_name = $fname;
					} else {
					    $full_name = "{$fname} {$lname}";
					}
				?>
				<div class="author vcard">
					<h1 class="author-name _color"><?php echo $full_name; ?>
					</h1>
					<span class="author-title _color"><?php echo $user_title; ?></span>
					<div class="author-infos">
						<div class="author-social">
							<?php if ( $user_instagram != '' || $user_pinterest != ''|| $user_email != ''){ ?>
							  	<?php if ( $user_instagram != '' ){ ?>
									<a href="<?php echo $user_instagram; ?>" class="sprite sprite-instagram" target="_blank"></a>
								<?php } ?>
								<?php if ( $user_pinterest != '' ){ ?>
									<a href="<?php echo $user_pinterest; ?>" class="sprite sprite-pinterest" target="_blank"></a>
								<?php } ?>
								<?php if ( $user_email != '' ){ ?>
									<a href="mailto:<?php echo $user_email; ?>" class="sprite sprite-email" target="_blank"></a>
								<?php } ?>
							<?php } ?>
						</div>
						<div class="author-data">
							<div class="author-avatar">
								<?php echo get_avatar(get_the_author_meta( 'user_email'),80); ?>
							</div>
							<p class="author-description full"><span><?php if ( $user_descr != '' ){ echo $user_descr; } ?></span></p>
						</div>
					</div>
				</div>
			</div>
			<div class="main-content">
				<?php $latest_cat_post = new WP_Query( array('post_type' => 'post','posts_per_page' => 10,'paged'=>$paged,'category__in' => get_query_var('cat'), 'author' =>  get_the_author_meta( 'ID' )));		
					if( $latest_cat_post->have_posts() ) :
						$i = 0; 
						while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
								$i++;
								get_template_part( 'templates/template_category' );
						endwhile;
					endif;
				?>
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</main>
<?php get_footer(); ?>