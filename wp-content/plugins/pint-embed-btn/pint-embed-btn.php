<?php

/*
Plugin Name: Pinterest Embed Button
Plugin URI: 
Description: Adds a pintembed button to visual editor.
Author: Elodie Buski
*/

function enqueue_plugin_scripts($plugin_array)
{
    //enqueue TinyMCE plugin script with its ID.
    $plugin_array["pint_embed_plugin"] =  plugin_dir_url(__FILE__) . "index.js";
    return $plugin_array;
}

add_filter("mce_external_plugins", "enqueue_plugin_scripts");

function register_buttons_editor($buttons)
{
    //register buttons with their id.
    array_push($buttons, "pint");
    return $buttons;
}

add_filter("mce_buttons", "register_buttons_editor");