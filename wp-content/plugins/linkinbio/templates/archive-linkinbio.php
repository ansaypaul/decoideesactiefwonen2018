<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(''); ?></title>
        <link rel="author" href="humans.txt">
        <?php wp_head(); ?>
       
    </head>
    <body class="linkinbio">
    	<header id="header" class="header header-linkinbio clear" role="banner">
			<div class="site-inner">
				<a href="<?php echo home_url(); ?>/" class="site-logo">
					<img src="<?php echo get_field('linkinbio_website_image','option' )?>" alt="ELLE logo">
				</a>
			</div>
		</header>
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
		<main role="main" class="main main-linkinbio">
			<div class="site-inner">
				<p><?php echo get_field('linkinbio_intro_phrase','option' )?></p>
				<div class="content entry-content">
					<ul class="insta-wrapper">
					<?php 
						$linkinbio = get_posts(array('post_type' => 'linkinbio', 'posts_per_page'=>30, 'post__not_in' => $excluded_ID, 'orderby' => 'date', 'order' => 'DESC'));
						foreach ( $linkinbio as $post ) : setup_postdata( $post ); $excluded_ID[] = $post->ID;?>
							<li class="insta-block">
								<?php 
					 				$insta_link = get_field( "instagram_pic_link", $post->ID );
					 				$relative_link = get_field( "relative_link", $post->ID );
					 				echo "<a class='insta-item' target='blank_' href='".$relative_link."'><img src=".$insta_link['sizes']['instagram_linkinbio']." alt='".$insta_link["caption"]."'></a>";
					 			?>
							</li>
						<?php endforeach; wp_reset_postdata();?>
					</ul>
				</div>
			</div>
		</main>
		<footer id="footer" class="footer footer-linkinbio" role="contentinfo">
			<div class="site-inner">
				<small>© <a href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a> <?php echo date("Y"); ?></small>
			</div>
		</footer>
    </body>
    <?php wp_footer(); ?>
</html>