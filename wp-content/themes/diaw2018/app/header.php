<!doctype html>
<html <?php language_attributes(); ?> >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="msvalidate.01" content="D7494E6276EFD8560757410AB05C906E" />
		<!-- FAVICON -->
		<?php 
			if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ ?>
				<link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/img/favicon/di/favicon-32x32.png" sizes="32x32">
				<link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/img/favicon/di/favicon-16x16.png" sizes="16x16">
			<?php } else { ?>
				<link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/img/favicon/aw/favicon-32x32.png" sizes="32x32">
				<link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/img/favicon/aw/favicon-16x16.png" sizes="16x16">
			<?php }; 
		?>
       
		<!--<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>-->
		<meta property="fb:pages" content="173570092719065">
		<?php wp_head(); ?> 
		<!-- Hotjar Tracking Code for http://www.decoidees.be/ -->

		<script type="text/javascript">
			var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>",
	        adposition = "<?php echo mb_strtoupper(ad_position()['cat']); ?>",
	        adpositionclean = "<?php echo ad_position()['cat']; ?>",
	        adpositiongemius = "<?php echo stripAccents(ad_position()['cat']); ?>",
	        adpositionpebble = "<?php echo ad_position()['pebble']; ?>",
	        adpositionpebble_categories = "<?php echo ad_position()['pebble_categories']; ?>",
	        adpositionbis = "<?php echo html_entity_decode(ad_position()['subcat']); ?>",
	        adpositionter = "<?php echo html_entity_decode(ad_position_ter()); ?>",
	        adpositiontype = "<?php echo ad_position()['type']; ?>",
	        id_post = "<?php echo get_the_ID(); ?>",
	        id_author = "<?php the_author(); ?>",
	        article_title = "<?php echo html_entity_decode( get_the_title(), ENT_QUOTES, 'UTF-8' ); ?>",
	        article_link = "<?php the_permalink(); ?>",
	        article_thumb = "<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>",
	        article_category = "<?php echo substr(get_category_article()['list_categories'],0,-1); ?>",
	        article_date = "<?php echo get_the_date('r'); ?>",
	        article_date_ga = "<?php echo get_field('date_de_creation'); ?>",
			article_tag = "<?php echo htmlspecialchars_decode(substr(get_category_article()['list_tags'],0,-1)); ?>",
			article_query_search = "<?php echo get_search_query(); ?>",
	        lang_identifiant = "<?php echo get_field('variable_js','option')['lang_identifiant']; ?>",
	 		lang_identifiantPebble = "<?php echo get_field('variable_js','option')['lang_identifiantpebble']; ?>",
	 		lang_identifiantGoogleAnalytics = "<?php echo get_field('variable_js','option')['lang_identifiantgoogleanalytics']; ?>",
	 		id_google_analytics_specifique = "<?php echo get_field('variable_js','option')['id_google_analytics_specifique']; ?>",
	 		id_name_ga = "<?php echo get_field('variable_js','option')['id_name_ga']; ?>",
	 		lang_identifiantGemius = "<?php echo get_field('variable_js','option')['lang_identifiantgemius']; ?>",
	 		lang_identifiantFacebookSDK= "<?php echo get_field('variable_js','option')['lang_identifiantfacebooksdk']; ?>",
	 		id_facebook_pixel = "<?php echo get_field('variable_js','option')['id_facebook_pixel']; ?>",
	 		no_skimlink_script = "<?php echo get_field('no_skimlink_script',get_the_ID()); ?>",
	        height,
	        width; 

	        var init_GPDR = "<?php echo GDPR ?>";
	        console.log('Init de la langue : ' + lang_identifiant);
        </script>
    </head>
    <body <?php body_class(); ?>>
<?php 
$query = new WP_Query(array( 'post_type' => array('cover'),'posts_per_page'=>'1'));
if ( $query->post_count >= 1) 
{
while ( $query->have_posts() ) : $query->the_post();
$cover_color = get_post_meta($post->ID,'color',false)[0];
wp_reset_postdata();
endwhile;
}                
echo '<style>
.swiper-pagination-bullet-active {background : '.$cover_color.' !important}
._background { background : '.$cover_color.' !important}._color { color : '.$cover_color.' !important}
._border { border-color : '.$cover_color.' !important}.content a { color : '.$cover_color.' !important}
.related-posts {display:block !important;}
</style>';               
?> 
   		<div class="search-wrapper">
   			<div class="search-closebtn"><span></span></div>
   			<form role="search" method="get" id="search-form" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
   				<input class="search-input _color" name="s" autocomplete="off" placeholder="<?php _e( "Rechercher", "html5blank" ); ?>..." type="text" value="<?php echo get_search_query(); ?>">
				<input class="search-btn _background" value="<?php _e( "Rechercher", "html5blank" ); ?>" type="submit" value="<?php echo esc_attr_x( 'recherche', 'submit button' ); ?>">
			</form>
   		</div>

   		<div class="backtotop">
   			<div class="div-before _background"></div>
   			<div class="div-after _background"></div>
   		</div>
   		<header id="header" class="header fixed">
				<?php if( is_home() OR is_page('index') ): ?>
				    <div class="swiper-container-test">
				    	<div class="container">
					    	<a class="link-logo _background" href="<?php echo home_url(); ?>">
			   					<img class="logo" src="<?php bloginfo("template_directory"); ?>/img/<?php _e( "DecoIdees_logo_b.svg", "html5blank" ); ?>" alt="<?php _e( "Déco idées", "html5blank" ); ?> logos">
			   					<div class="div-after _background"></div>
			   				</a>
		   				</div>
				        <div class="swiper-wrapper">
				        	<?php global $excluded_ID; ?>
							    <?php
							     if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
							     	$the_query = new WP_Query( array('category__in' => array( 2033 ),
							     	 'posts_per_page' => 4 ) ); 
									} else { 
									$the_query = new WP_Query( array('category__in' => array( 1271 ), 
										'posts_per_page' => 4 ) ); 
								}
								$i = 0; 
							     ?>
							        <?php if ( $the_query->have_posts() ) : ?>
							        	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); $i++; ?>
							        		<?php $excluded_ID[] = get_the_ID(); ?>
							        		<?php $categories = get_the_category(); ?>
									  		<div class="swiper-slide">
												<div class="container">
													<article>
											    		<span class="category "><a class="_background" href="<?php echo esc_url(get_category_link($categories[0]->term_id )) ?>"><?php  echo esc_html( $categories[0]->name );  ?></a></span>
											    		<?php if($i != 1){ ?>
														<h2><a href="<?php the_permalink(); ?>" class="link-article"><?php the_title(); ?></a></h2>
														<?php } else{ ?>
														<h1><a href="<?php the_permalink(); ?>" class="link-article"><?php the_title(); ?></a></h1>

														<?php }?>
														<?php if ( ! has_excerpt() ) {
														      echo '';
														} else { 
														      the_excerpt();
														} ?>
											    	</article>
												</div>
												<div class="cover">
													<?php the_post_thumbnail('slider',['class' => 'no-lazyload']); ?>
												</div>
											</div>
										<?php endwhile; ?>
								<?php endif; ?>
				        </div>
					</div>
		        <?php endif; ?>
        	<div class="menu-wrapper <?php 
				if ( !is_home() ) {
				  	echo 'fixed';
				};
			?>"> 
	        	<div class="menu _border _background <?php 
				if ( !is_home() ) {
				  	echo 'fixed';
				};
			?>"> 
					<div class="container-main">
						<nav>
		        		<ul>
		        			<li class="logo">
		        				<a href="<?php echo home_url(); ?>">
		        					<img class="img-mobile" src="<?php bloginfo("template_directory"); ?>/img/<?php _e( "DI_LOGO2014_white.png", "html5blank" ); ?>">
		        					<img class="img-desktop" src="<?php bloginfo("template_directory"); ?>/img/<?php _e( "DI_LOGO2014.png", "html5blank" ); ?>">
		        				</a>
		        			</li>
		        			<li class="burger"><span></span></li>
		        			<li class="responsive">

		        				<?php wp_nav_menu( 
	        						array( 
	        							'theme_location' => 'main-menu', 
	        							'container' => '',
	        							'items_wrap'      => '<ul class="responsive-ul">%3$s',
	        							'link_before' => '<span>',
                    					'link_after' => '</span><span></span>'
	        						) 
	        					); ?>
				        			<li class="lang hover-effect">
				        				<ul class="lang-ul">
				        					<li><a href="<?php _e( "http://www.decoidees.be/", "html5blank" ); ?>" class="hover-effect"><span class="_color"><?php _e( "FR", "html5blank" ); ?></span><span class="_color"><?php _e( "FR", "html5blank" ); ?></span></a></li>
				        					<li><a href="<?php _e( "http://www.actiefwonen.be/", "html5blank" ); ?>" class="hover-effect"><span><?php _e( "NL", "html5blank" ); ?></span><span><?php _e( "NL", "html5blank" ); ?></span></a></li>
				        				</ul>
				        			</li>
				        			<li class="search hover-effect">
				        				<img src="<?php bloginfo("template_directory"); ?>/img/search.png" alt="Search">
				        				<img src="<?php bloginfo("template_directory"); ?>/img/search-g.png" alt="Search">
				        			</li>
				        			<li class="social hover-effect">
				        				<a href="<?php echo get_field('socials_links','option')['facebook']; ?>" target="_blank" alt="Facebook">
				        					<img src="<?php bloginfo("template_directory"); ?>/img/facebook.png">
				        					<img src="<?php bloginfo("template_directory"); ?>/img/facebook-g.png">
				        				</a>
				        				<a href="<?php echo get_field('socials_links','option')['instagram']; ?>" target="_blank" alt="Instagram">
				        					<img src="<?php bloginfo("template_directory"); ?>/img/instagram.png">
				        					<img src="<?php bloginfo("template_directory"); ?>/img/instagram-g.png">
				        				</a>
				        			</li>
		        				</ul>
		        			</li>
		        			<!-- COVER PIC CSS -->
		        <?php 
                    $query = new WP_Query(array( 'post_type' => array('cover'),'posts_per_page'=>'1'));
                    if ( $query->post_count >= 1) 
                    {
                        while ( $query->have_posts() ) : $query->the_post();
                        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                        $title = get_the_title();
                        global $cover_color;
                        $cover_color = get_post_meta($post->ID,'color',false)[0];
                        ?>
                        <li class="store">
		        				<a href="<?php _e( "https://www.viapress.be/magazine-deco-idees.html", "html5blank" ); ?>" target="_blank"><div><img id="cover_ori_FR" src="<?php echo $url; ?>" alt="cover"></div>
		        				<span><?php _e( "le n°", "html5blank" ); ?><?php echo $title ;?><br><?php _e( "est en librairie", "html5blank" ); ?></span></a>
		        		</li>
                       <?php
                        wp_reset_postdata();
                        endwhile;
                        } 
                ?> 
		        		</ul>
		        		</nav>
		        	</div>
	        	</div>
        	</div>
		</header>
	<section class="main-wrapper">
	
	<div id="billboard" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="MOB640x150" data-refadMOB="pebbleMOB640x150" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position=""></div>