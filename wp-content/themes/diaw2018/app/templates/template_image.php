<!-- single.php -->
<?php 
	$parent_post = get_post($post->post_parent);  
	$title = $parent_post->post_title;
	$parent_link = get_permalink($parent_post->ID);
	$categories = get_the_category($post->post_parent);
	$categorie = $categories[0];
	$category_link = get_category_link( $categorie->cat_ID );
?>
<div id="container" class="container-main">
	<article>
		<div class="full-content">
			<h1 class="post-title"><?php echo $title; ?></h1>
		</div>
		<div class="content">
		<?php 	
			$_galleries = get_post_galleries($parent_post, false );
			$gallery = 0;
			$_gallery = 0;
			$galleries = array();
	
			for($i = 0; $i < count($_galleries); $i++){
				if(!isset($_galleries[$i]['link'])) {
					$_gallery = explode(',', $_galleries[$i]['ids']);
					for ($y=0; $y < count($_gallery); $y++) { 
						if($_gallery[$y] == $post->ID) {
							$gallery = $_gallery;
							break;
						}
					}
				} 
			}

			if ( count( $gallery ) > 1 ) {
				for($i = 0; $i < count($gallery); $i++){
					if($post->ID == $gallery[$i]) {
						$number_current = $i;
						$previous_attachment_url = $gallery[$i - 1] ? get_attachment_link($gallery[$i - 1]) : get_attachment_link($gallery[count($gallery) - 1]);
						$next_attachment_url = $gallery[$i + 1] ? get_attachment_link($gallery[$i + 1]) : get_attachment_link($gallery[0]);
					}
				}
			}
		?>
			<div class="img-gallery-container">
				<a href="<?php echo $parent_link; ?>" class="btn-back _background"><?php _e( "Retour", "html5blank" ); ?></a>
				<span class="img-counter _background"><?php echo $number_current+1 ?>/<?php echo count($gallery) ?></span>
				<a class="previous image-control" href="<?php echo esc_url( $previous_attachment_url ); ?>">
					<span class="_background control-top"></span>
					<span class="_background control-bottom"></span>
				</a>
				<a class="next image-control" href="<?php echo esc_url( $next_attachment_url ); ?>">
					<span class="_background control-top"></span>
					<span class="_background control-bottom"></span>
				</a>
				<?php echo wp_get_attachment_image( get_the_ID(), 'd_gallery' ); ?>
			</div>
			<div class="img-legend">
				<div class="titre"><?php echo get_the_excerpt(); ?></div>
				<div class="prix"><a target="_blank" href="<?php  echo get_the_content();  ?>"><?php echo get_post_meta(get_the_ID(),'_wp_attachment_image_alt', true); ?></a></div></div>
			</div>
			<!-- <div class="post-author _color">par <span><?php the_author(); ?></span></div> -->
		<?php get_sidebar(); ?>
	</article>
</div>