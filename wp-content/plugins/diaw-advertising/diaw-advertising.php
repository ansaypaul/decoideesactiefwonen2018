<?php
/**
 * Plugin Name: Déco Idées Advertising
 * Description: Advertising
 * Version: 1.0.0
 * Author: Laurent Fulster
 * Author URI: mailto:laurent.fulster@gmail.com
 */

function diaw_ad( $ad_format ) {
	$ad_position    = ad_position();
	$ad_position_kw = ad_position_kw();

	if ( $ad_format == 'atf' ) {
?>
<script src="//www.googletagservices.com/tag/js/gpt.js">
console.log('ATF');
	if ( width >= 1128 ) {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_ATF',[[1,1],[728,90],[840,150],[970,250]]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	} else if ( width >= 728 ) {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_ATF',[728,90]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	} else {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_ATF',[640,150]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	}
</script>
<?php
	} else if ( $ad_format == 'atf-2' ) {
?>
<script src="//www.googletagservices.com/tag/js/gpt.js">
	if ( width >= 1300 ) {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_ATF',[160,600]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	}
</script>
<?php
	} else if ( $ad_format == 'btf' ) {
?>
<script src="//www.googletagservices.com/tag/js/gpt.js">
	if ( width >= 768 ) {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_BTF',[[300,250],[300,600]]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	} else {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_BTF',[600,500]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	}
</script>
<?php
	} else if ( $ad_format == 'mtf' ) {
?>
<script src="//www.googletagservices.com/tag/js/gpt.js">
	if ( width >= 768 ) {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_MTF',[[1,1],[500,400]]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	} else {
		googletag.pubads().definePassback('/117318597/DECOIDEES_FR_MTF',[[1,1],[640,960]]).setTargeting('position',<?php echo $ad_position; ?>).setTargeting('kw',<?php echo $ad_position_kw; ?>).display();
	}
</script>

<?php }
}

function ad_position() {
	$position_id = "others";
	if ( $_SERVER['REQUEST_URI'] == '/' ) {
		$position_id = "homepage";
	}

	return "'" . $position_id . "'";
}

function ad_position_kw() {
	if ( is_category() ) {
		$cat_id = get_query_var( 'cat' );
		$category_parent = get_category( $cat_id );
		$kw_id = "'" . $cat_id . "'";
		if ( $category_parent->category_parent != 0 ) {
			$kw_id = "['" . $category_parent->category_parent . "', " . $kw_id . "]";
		}
	} else if ( is_tag() ) {
		$kw_id = "'" . get_query_var( 'tag_id' ) . "'";
	} else if ( is_single() || is_attachment() ) {
		$post_id = '';
		if ( is_attachment() ) {
			$post_id = get_post()->post_parent;
		}
		if ( $categories = get_the_category( $post_id ) ) {
			$count = count( $categories );
			foreach( $categories as $category ) {
				$kw_id .= "'" . $category->term_id . "',";
			}
			if ( $posttags = get_the_tags( $post_id ) ) {
				foreach( $posttags as $tag ) {
					$kw_id .= "'" . $tag->term_id . "',";
				}
			}
			$kw_id = substr( $kw_id, 0, -1 );
			if ( $count > 1 || $posttags ) {
				$kw_id = "[" . $kw_id . "]";
			}
		} else {
			$kw_id = "'0'";
		}
	} else if ( is_page() && $_SERVER['REQUEST_URI'] != '/' ) {
		$kw_id = "'" . get_the_ID() . "'";
		if ( get_post()->post_parent != 0 ) {
			$kw_id = "['" . get_post()->post_parent . "'," . $kw_id . "]";
		}
	} else {
		$kw_id = "'0'";
	}

	return $kw_id;
}

add_action( 'wp_enqueue_scripts', function() {
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-1.11.3.min.js', array(), '1.11.3', false );
	wp_enqueue_script( 'jquerymigrate', 'https://code.jquery.com/jquery-migrate-1.2.1.min.js', array( 'jquery' ), '1.2.1', false );
} );

add_action( 'wp_head', function() { ?>
<script>
	var adposition   = "<?php echo ad_position(); ?>",
		adpositionkw = "<?php echo ad_position_kw(); ?>",
		height,
		width;

	if ( typeof window.innerWidth != 'undefined' ) {
		height = window.innerHeight;
		width = window.innerWidth;
	} else if ( typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0 ) {
		height = document.documentElement.clientHeight;
		width = document.documentElement.clientWidth;
	} else {
		height = document.getElementsByTagName( 'body' )[0].clientHeight;
		width = document.getElementsByTagName( 'body' )[0].clientWidth;
	}
</script>
<?php }, 40 );

add_action( 'wp_footer', function() { ?>

<?php } );
