<!-- category -->
<article class="cat-article">
	<figure>
	<div class="cat-category _background">
		<?php
		if(is_category()){
			single_cat_title();
		}
		else{
			$category = get_the_category();
			$cat_tree = get_category_parents($category[0]->term_id, FALSE, ':', TRUE);
			$top_cat = explode(':',$cat_tree);
			$parent = $top_cat[0];
			echo $parent;
		}
		?>
	</div>
	<a href="<?php the_permalink() ?>" class="link-img"><?php the_post_thumbnail('d_category'); ?></a>
		<figcaption>
			<div class="content-box">
				<div class="cat-info">
					<time class="cat-date" datetime="<?php the_time('Y-m-d') ?>"><?php the_time('d.m.Y') ?></time>
					<span class="tags _color">Tags</span>
					<?php
						$categories = wp_get_post_categories( $post->ID );
						if ( $categories[1]=="") {
							$posttags = wp_get_post_tags( $post->ID );
						    if ($posttags) {
						      foreach($posttags as $tag) {
						      	echo "<span class='cat-tags'>". $tag->name ."</span>";
						      }
						    }
						} else {
							$all_cats_ancestror = array();
							foreach ( $categories as $cat ) { 
								$ancestors = get_ancestors( $cat, 'category' );
								if ( empty( $ancestors ) ) 
									$all_cats_ancestror[] = $cat; // Parents
								else 
									echo "<span class='cat-tags'>".get_cat_name($cat)."</span>"; // Enfants
							}
						}
					?>
				</div>
				<a href="<?php the_permalink() ?>" class="cat-link"><h2 class="cat-title"><?php the_title(); ?></h2></a>
				<p><?php echo get_post_meta($post->ID, 'sous-titre', false )[0] ?></p>
				<div class="cat-author _color"><?php _e( "par", "html5blank" ); ?> <span><?php the_author(); ?></span></div>
			</div>
		</figcaption>
	</figure>
</article>