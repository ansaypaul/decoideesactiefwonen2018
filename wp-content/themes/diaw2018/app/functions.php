<?php 
	/*------------------------------------*\
	    External Modules/Files
	\*------------------------------------*/

	// Load any external files you have here
	//require_once('include/maintenance.php');
	//die();
	error_reporting(0);

	require_once('include/custom_post.php');
	require_once('include/acf.php');

	function load_custom_scripts() {
	    wp_deregister_script( 'jquery' );
	    wp_register_script('jquery', get_template_directory_uri() . '/js/lib/jquery-2.2.4.min.js', array(), '2.2.4', true); // true will place script in the footer
	    wp_enqueue_script( 'jquery' );
	}
	if(!is_admin()) {
	    add_action('wp_enqueue_scripts', 'load_custom_scripts', 99);
	}

	// Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');

	/**
	 * Disable the emoji's
	 */
	function disable_emojis() {
	 remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	 remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	 remove_action( 'wp_print_styles', 'print_emoji_styles' );
	 remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	 remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	 remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	 remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	 add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	 add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
	}
	add_action( 'init', 'disable_emojis' );

	/**
	 * Filter function used to remove the tinymce emoji plugin.
	 * 
	 * @param array $plugins 
	 * @return array Difference betwen the two arrays
	 */
	function disable_emojis_tinymce( $plugins ) {
	 if ( is_array( $plugins ) ) {
	 return array_diff( $plugins, array( 'wpemoji' ) );
	 } else {
	 return array();
	 }
	}

	/**
	 * Remove emoji CDN hostname from DNS prefetching hints.
	 *
	 * @param array $urls URLs to print for resource hints.
	 * @param string $relation_type The relation type the URLs are printed for.
	 * @return array Difference betwen the two arrays.
	 */
	function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	 if ( 'dns-prefetch' == $relation_type ) {
	 /** This filter is documented in wp-includes/formatting.php */
	 $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

	$urls = array_diff( $urls, array( $emoji_svg_url ) );
	 }

	return $urls;
	}

	/* MUTLIPAGE */

	add_filter('the_content', 'replace_pagebreak');  

	function replace_pagebreak( $content ) {
	    $content_new = str_replace("<!--nextpage-->","[multipage_block]",$content);
	    return $content_new;
	}   

	function bloc_multipage_shortcode() {
	    global $multipage_number_current;
	    $multipage_number_current++; 
	    $multiblock_ads_randorm = rand(1,999999999);
	    $multipage_block = '';
	    if($multipage_number_current > 1 && ($multipage_number_current == 2 || $multipage_number_current == 4 || $multipage_number_current == 6)){ 
	           /* $multipage_block .= '<div id="billboard'.$multiblock_ads_randorm.'" class="ads" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="" data-position="2"></div>';*/
	    }
	    $multipage_block .= '<div class="segment-multipage"><span class="current-multipage">'.$multipage_number_current.'</span></div>';
	    return $multipage_block;
	}

	add_shortcode('multipage_block', 'bloc_multipage_shortcode');

	function redirect_302(){
	    if(is_single()){
	      global $page;
	      if($page > 1){
	        $url = get_permalink();
	        echo 'TESTPAGE:'.$page.$url ;
	        wp_redirect($url,302);
	        exit;
	      }
	    }
	  }

	add_action( 'wp_head', function() { 
	  redirect_302();
	});
	/* */

	//Call CSS
	function theme_css() {
		//wp_enqueue_style( 'swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.css',array(),'5.0.2', false);
		wp_enqueue_style( 'site', get_template_directory_uri() . '/css/style.min.css',array(),'5.0.2', false);
		//wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', false);
		wp_enqueue_style( 'swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css', false);
	}
	add_action( 'wp_enqueue_scripts', 'theme_css' );

	//Call Javascript
	function theme_js(){
		wp_enqueue_script('c','//c.pebblemedia.be/js/c.js', array('jquery'), '1.0.0', true); // picturefill
	    wp_enqueue_script( 'tag', 'https://pool-pebblemedia.adhese.com/tag/tag.js', false );
        wp_enqueue_script('postscribe', get_template_directory_uri() . '/js/lib/postscribe.js', array('jquery'), '1.0.0', true); // picturefill
	    wp_enqueue_script( 'main', get_bloginfo('template_directory').'/js/main.js', false );
	    wp_enqueue_script( 'swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js', array(), '4.7.2', false );
	   	wp_enqueue_script( 'pinterest-embed', '//assets.pinterest.com/js/pinit.js', array(), '1.0.0', false );
	}
	add_action( 'wp_footer', 'theme_js' );

	// Ajoutes la fonction image à la une
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	//Supprimer les hauteur et largeur par défaut sur les images
	add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
	add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

	function remove_width_attribute( $html ) {
	   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	   return $html;
	}


	function _display_id_random(){
	        $multiblock_ads_randorm = rand(1,999999999);
	        return $multiblock_ads_randorm;
	}

	function display_id_random(){
	        $multiblock_ads_randorm = rand(1,999999999);
	        echo $multiblock_ads_randorm;
	}


	//Gérer la longueur de l'extrait
	function custom_excerpt_length( $length ) {
		return 10;
	}
	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

	//Gérer le more de l'extrait
	function wpdocs_excerpt_more( $more ) {
    return ' ...';
	}
	add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

	//Ajouter un menu dynamique
	function register_my_menu() {
	  register_nav_menu('main-menu',__( 'Menu perso' ));
	}

	add_action( 'init', 'register_my_menu' );

	/* <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?s> */

	function my_img_caption_shortcode_filter( $val, $attr, $content = null ) {
	  extract( shortcode_atts( array(
	    'id'      => '',
	        'align'   => 'aligncenter',
	        'width'   => '',
	        'caption' => ''
	    ), $attr ) );
	    
	    // No caption, no dice...
	    if ( 1 > (int) $width || empty( $caption ) )
	        return $val;
	 
	    if ( $id )
	        $id = esc_attr( $id );
	     
	    // Add itemprop="contentURL" to image - Ugly hack
	    $content = str_replace('<img', '<img itemprop="contentURL"', $content);
	    return '<figure id="' . $id . '" aria-describedby="figcaption_' . $id . '" class="wp-caption ' . esc_attr($align) . '" itemscope itemtype="http://schema.org/ImageObject">' . do_shortcode( $content ) . '<figcaption id="figcaption_'. $id . '" class="wp-caption-text" itemprop="description">' . $caption . '</figcaption></figure>';
	}

	add_filter( 'img_caption_shortcode', 'my_img_caption_shortcode_filter', 10, 3 );

	//Créer des formats d'images différents
	if ( function_exists( 'add_image_size' ) ) { 
		add_image_size( 'd_slider', 2000, 1500, true );
		add_image_size( 'd_news', 300, 250, true );
		add_image_size( 'd_full', 1100, 340, true );
		add_image_size( 'd_single', 1200, 540, true );
		add_image_size( 'd_single_n', 1000, 650, true );
		add_image_size( 'd_gallery', 850, 550, true );
		add_image_size( 'd_category', 820, 685, true );
		add_image_size( 'd_swiper', 9999,550, false);
	}

	//Adding custom social fields on profile page
	add_filter( 'user_contactmethods', function( $user_contact ) {
	  $user_contact[ 'authortitle' ] = 'Titre/fonction';
	  $user_contact[ 'pinterest' ] = 'Pinterest profile URL';
	  $user_contact[ 'instagram' ] = 'Instagram profile URL';

	  unset( $user_contact[ 'aim' ] );
	  unset( $user_contact[ 'jabber' ] );
	  unset( $user_contact[ 'yim' ] );
	  unset( $user_contact[ 'facebook' ] );
	  unset( $user_contact[ 'twitter' ] );
	  unset( $user_contact[ 'googleplus' ] );
	  unset( $user_contact[ 'siteweb' ] );

	  return $user_contact;
	} );

	/* Autoriser les fichiers SVG */
	function wpc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'wpc_mime_types'); 

	/*Ajouter la categorie en class*/
	function add_class_callback( $result ) {

	  $class = strtolower( $result[2] );
	  $class = str_replace( ' ', '-', $class );
	  // do more sanitazion on the class (slug) like esc_attr() and so on

	  $replacement = sprintf( ' class="%s">%s</a>', $class, $result[2] );
	  return preg_replace( '#>([^<]+)</a>#Uis', $replacement, $result[0] );

	}

	function add_category_slug( $html ) {

	  $search  = '#<a[^>]+(\>([^<]+)\</a>)#Uuis';
	  $html = preg_replace_callback( $search, 'add_class_callback', $html );

	  return $html;
	}

	add_filter( 'the_category', 'add_category_slug', 99, 1 );

	if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Catégories',
		'id' => 'Catégories',
		'before_title' => '<h1>',
		'after_title' => '</h1>',
	)); }

	//Sort posts by "Popular"
	function wpb_set_post_views($postID) {
		$count_key = 'wpb_post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}
	//To keep the count accurate, lets get rid of prefetching
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

	//Pagination
	function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a class="_color" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a class="_color" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a class="_color" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="_color"';
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

 /************************** FACEBOOK PIXEL TRACKING *****************************/
function trackingFacebook(){
    if(is_single()){
        $slug = get_post_field( 'post_name', get_post() ); 
        $categories = get_the_category(get_the_ID());
        $tags =  wp_get_post_tags(get_the_ID());

        foreach ($categories as $categorie) {
            $list_categories .=  $categorie->name . ', ';
        }

        foreach ($tags as $tag) {
            $list_tags .=  $tag->name . ', ';
        }
    ?>
    <script>fbq('track', 'ViewContent', {title: "<?php echo $slug ?>",url: "<?php echo the_permalink(); ?>"});</script>
    <script>fbq('track', 'ViewCategory', {title: "<?php echo htmlspecialchars_decode($list_categories) ?>"});</script>
    <script>fbq('track', 'ViewTag', {title: "<?php echo htmlspecialchars_decode($list_tags) ?>"});</script>

    <?php
    }
    else if(is_category() || is_archive()){ 
    $current_category = single_cat_title("", false); 
    ?>
    <script>fbq('track', 'ViewCategory', {title: "<?php echo htmlspecialchars_decode($current_category) ?>"});</script>
    <?php
    }
    else if(is_tag()){ 
    $current_tag = single_tag_title("", false); ?> 
    <script>fbq('track', 'ViewTag', {title: "<?php echo htmlspecialchars_decode($current_tag) ?>"});</script>
    <?php
    }
    else if(is_page()){ 

    }
    else if(is_search()){  ?>
    <script>fbq('track', 'Search', {search_string: '<?php echo $_GET['s'] ?>'});</script>
    <?php }

}
//add_action( 'wp_footer', function() {trackingFacebook();} );
/////////////////////**************************************************/

function stripAccents( $str, $charset = 'utf-8' ) {
    $str = htmlentities( $str, ENT_NOQUOTES, $charset);
    $str = preg_replace( '#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $str );
    $str = preg_replace( '#&([A-za-z]{2})(?:lig);#', '\1', $str );
    $str = preg_replace( '#&[^;]+;#', '', $str );
    return $str;
}

/** Fonction article **/
function get_category_article(){
        if(is_single()){
            $slug = get_post_field( 'post_name', get_post() ); 
            $categories = get_the_category(get_the_ID());
            $tags =  wp_get_post_tags(get_the_ID());
            $info_article['list_categories'] = '';
            $info_article['list_tags'] = '';
            $list_categories = '';
            $list_tags = '';
            foreach ($categories as $categorie) {
                $list_categories .=  $categorie->name . ',';
            }
            if(isset($list_categories) && $list_categories != ''){ $info_article['list_categories'] = $list_categories; }
            foreach ($tags as $tag) {
                $list_tags .=  $tag->name . ',';
            }
            if(isset($list_tags) && $list_tags != ''){ $info_article['list_tags'] = $list_tags; }
            return $info_article;
        }else{
            $info_article['list_categories'] = '';
            $info_article['list_tags'] = '';
            return $info_article;
        }
}

function ad_position($post_id = ''){ 
    global $post;

    if ( is_home() && is_front_page() ) {
        /** HOME PAGE **/
        $ad_position['cat'] = 'HOME';
        $ad_position['subcat'] = 'HOME';
        $ad_position['pebble'] = 'homepage';
        $ad_position['type'] = ' (Navigation)';
    	}else {
        /** CATEGORIE ARTICLE **/
        $ad_position['cat'] = 'OTHER';
        $ad_position['subcat'] = 'OTHER';
        $ad_position['pebble'] = 'others';
        $ad_position['type'] = ' (Article)';
    	}
    return $ad_position;
}

function ad_position_ter() {
    $tag_id = '';
    if ( is_tag() ) {
        $tag_id = single_tag_title( '', false );
    }
    else if ( is_single() || is_attachment() ) {
        $posttags = get_the_tags();
        if ( $posttags ) {
            foreach( $posttags as $tag ) {
                $tag_id .= $tag->name .', ';
            }
            $tag_id = substr( $tag_id, 0, -2 );
        }
        else {
            $tag_id = 0;
        }
    }
    else {
        $tag_id = 0;
    }

    return $tag_id;
}


/* SLIDER MOBILE */
add_action( 'wp_ajax_nopriv_ajax_load_article', 'my_ajax_load_article' );
add_action( 'wp_ajax_ajax_load_article', 'my_ajax_load_article' );

function my_ajax_load_article() {
    global $post;
    //$args = array('post_status' => array('publish'),'posts_per_page'=> 1);
    if(isset($_POST['cat'])){$args = array('post_status' => array('publish'),'posts_per_page'=> 1,'cat'=> $_POST['cat'],'post__not_in'=> $_POST['excluded_id']);}
    if(isset($_POST['tag'])){$args = array('post_status' => array('publish'),'posts_per_page'=> 1,'tag_id'=> $_POST['tag'],'post__not_in'=> $_POST['excluded_id']);}

    $custom_query = new WP_Query($args);
    while($custom_query->have_posts()) : $custom_query->the_post();  
    $list_categorie = get_the_category(); 
        foreach ($list_categorie as $categorie) {
            $list_categorie_name[] = $categorie->slug;
        }

		if(has_term('beau_et_bon', 'category', $post)) {      
		   get_template_part( 'templates/template_single_beau_et_bon' );
		} else if (has_term('leuk_en_lekker', 'category', $post)){
			get_template_part( 'templates/template_single_beau_et_bon' );
		} else if(has_term('diy-steps', 'category', $post)) {      
		   get_template_part( 'templates/template_single_diy_steps' );
		} else {
		   get_template_part( 'templates/template_single' );
		}     

    endwhile; wp_reset_postdata();
    die();
}



/* Ajout icone pour les articles */
function modify_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr = array()) {
        global $multipage;

        $id = get_post_thumbnail_id(); // gets the id of the current post_thumbnail (in the loop)
        if(isset($post_id)){$id = get_post_thumbnail_id($post_id);}
        if(get_field('image_verticale',$post_id)){$id = get_field('image_verticale',$post_id);}
        $src = wp_get_attachment_image_src($id, $size); // gets the image url specific to the passed in size (aka. custom image size)
        $src_prog = wp_get_attachment_image_src($id, 'content2018prog'); // gets the image url specific to the passed in size (aka. custom image size)
        $image_alt = get_the_title($post_id);
        $content = get_the_content($post_id);
        $list_categorie = get_the_category(); 
        foreach ($list_categorie as $categorie) {
            $list_categorie_name[] = $categorie->slug;
        }

        $icone = '';
            if($size != 'news-thumb' && $size != 'square' && $size != 'instagram'){
                /*$html = 
                '<picture class=" '.$attr[0].'">
                    <!--[if IE 9]><video style="display: none;"><![endif]-->';
                if($attr[0] == 'lazy'){ 
                    $html .= elle_get_picture_srcs( $id , $size);
                }else{
                    $html .= elle_get_picture_srcs_nolazyload( $id , $size);  
                }
                $html .='<!--[if IE 9]></video><![endif]-->
                    <img srcset="" src="'.$src_prog[0].'" alt="' . $image_alt . '">
                </picture>';*/
                if($attr[0] == 'lazy'){ 
                $image_src = wp_get_attachment_image_src(  $id, $size );
                $img_html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                $html = apply_filters( 'bj_lazy_load_html', $img_html );
                }else{
                $image_src = wp_get_attachment_image_src(  $id, $size );
                $html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                }
            }

            if(get_field("gif",$post_id)){
                 if($attr[0] == 'lazy'){ 
                    $html = '<img class="no-lazy-img lazy-img" src="'.get_template_directory_uri().'/img/blank.gif" data-srcset="'.get_field("gif",$post_id).'" alt="' . $image_alt . '">';
                }else{
                    $html = '<img class="" srcset="'.get_field("gif",$post_id).'" alt="' . $image_alt . '">';
                }
            }
                
            if($size == 'instagram'){
            $image_src = wp_get_attachment_image_src(  $id, 'instagram' );
                 if($attr[0] == 'lazy'){ 
                    $img_html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                    $html = apply_filters( 'bj_lazy_load_html', $img_html );
                }else{
                    $image_src = wp_get_attachment_image_src(  $id, 'instagram' );
                    $html = '<img class="" src="'.$image_src[0].'" alt="' . $image_alt . '">';
                }
            }
            
            if(count($attr) > 3){
                if($attr[3] == 1 && get_field("url_video",$post_id)){
                    //$html = '<div class="player_youtube_wrapper"><div class="player_youtube_container" id="player_youtube_container">'.get_field("url_video").'</div></div>';
                    $ID_video = explode('/', get_field("url_video",$post_id))[3];
                    $url_video = "http://www.youtube.com/embed/".$ID_video."?enablejsapi=1&autoplay=0&showinfo=0&controls=1&autohide=1"; 
                    if(is_ssl()){$url_video = "https://www.youtube.com/embed/".$ID_video."?enablejsapi=1&autoplay=0&showinfo=0&controls=1&autohide=1";}

                    $html = '<div class="player_youtube_wrapper"><iframe id="player" type="text/html" width="640" height="360" src="'.$url_video.'" frameborder="0"></iframe></div>';
                }
            }

            if($attr[1] == 1){ 

            }

            if($attr[2] == 1 && $attr[3] != 1){ 
                $html .= '<span class="caption-div">';
                $html .= get_the_post_thumbnail_caption($post_id);
                $html .= '</span>';
            }

      return $html;
}

add_filter('post_thumbnail_html', 'modify_post_thumbnail_html', 99, 5);
?>