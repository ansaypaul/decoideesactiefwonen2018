<!-- Related -->
<article class="post-related">
	<a href="<?php the_permalink(); ?>">
		<figure>
		<?php the_post_thumbnail('d_news'); ?>
			<figcaption>
				<?php foreach((get_the_category()) as $category) { ?>
					<?php if (!empty( $category ) && (!is_category()) && ($category != 'slider') ) { ?>
						<div class="related-category _color">
							<?php echo esc_html( $category->cat_name); ?>
						</div>
					<?php 
					break;
					}	?>
				<?php } ?>
				<h2 class="related-title"><?php the_title(); ?></h2>
			</figcaption>
		</figure>
	</a>
</article>