<?php get_header(); ?>
	<main class="main">
	<!-- 404 -->
		<div class="container-main">
			<div class="full-content">
				<h1 class="page-title"><?php _e( "Erreur 404", "html5blank" ); ?></h1>
			</div>
			<div class="content">
				<p class="p-404"><?php _e( "Oops, cette page est introuvable!", "html5blank" ); ?></p>
				<a class="a-404" href="<?php echo get_option('home'); ?>/"><img class="img-404" src="<?php bloginfo("template_directory"); ?>/assets/img/404.gif"></a>
				<a class="a-404" href="<?php echo get_option('home'); ?>/"><?php _e( "Retour sur la page d'accueil", "html5blank" ); ?></a>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</main>
<?php get_footer(); ?>