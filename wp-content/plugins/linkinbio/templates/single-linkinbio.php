<?php get_header(); ?>
	<main role="main" class="main single-linkinbio">
		<div class="site-inner single-linkinbio">
			<div class="content entry-content">
				<?php 
	 				$insta_link = get_field( "instagram_pic_link", $post->ID );
	 				$relative_link = get_field( "relative_link", $post->ID );
	 				echo "<a class='insta-item' target='blank_' href='".$relative_link."'><img src=".$insta_link["url"]." alt='insta'></a>";
	 			?>
			</div>
		</div>
	</main>
<?php get_footer(); ?>
