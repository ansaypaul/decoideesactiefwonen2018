<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'decoidees');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>[_qpC[|gOE%^MIrTT-H;cr9[*r$D`XdXdlz%@/-Jroa-sw(Qwau *TQ*!8mpNqh');
define('SECURE_AUTH_KEY',  'Sr~Z$[ApP~>naULz.{N!!Fz*ZW!,Nr408IA|l3mZyMq>CiqMu3-Z{tqARTO82OE:');
define('LOGGED_IN_KEY',    'DALr)76VdeXX.l-aJU@,]75CxNpsG_cHv^bxwD*d/3d9PrZx6(##]:$x=BB6jL{i');
define('NONCE_KEY',        't)y |*@WkV4<6n@.z=!@268<r%,s~);oYStEA(/T Rw>7{6F;R]z3L&=,b#>HZ0_');
define('AUTH_SALT',        '-zZ+N`p`A8JGW</MKwyKvzFb(t%^C}c,tj.Z!=nWM`8glcQ|PhbjhmSrHb]bwFLP');
define('SECURE_AUTH_SALT', '-J4`W1^hTRUbyyI8vj3GADVU7v;d.20Y3nOOrc}m:lk)j]J:q=W / wyZzL7>S$G');
define('LOGGED_IN_SALT',   'TX!(&m5clV~-4Ps88i7Hkn^;i/]~_W73<UM1cN7bCnab@89e=PkdWy1LtG ;=VkP');
define('NONCE_SALT',       '&e](vkiw.M7!wRB|Jy3.G_W>gU_F[li<Msv,%; 1[W]LAqUaT9z;9ZYFk{,.AMR^');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');