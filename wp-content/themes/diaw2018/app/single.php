<?php get_header(); ?>
	<main class="main">
		<?php
			/* Most popular*/
			$now   = time();
			$date_post = strtotime(get_the_date('Y-m-d'));
			$diff  = abs($now - $date_post);
			wpb_set_post_views(get_the_ID());
		?>
		<div id="container" class="container-main container-articles">
			<?php if ( have_posts() ) : ?>
	            <?php while ( have_posts() ) : the_post(); ?> 
	        		<?php        
						if(has_term('beau_et_bon', 'category', $post)) {      
						   get_template_part( 'templates/template_single_beau_et_bon' );
						} else if (has_term('leuk_en_lekker', 'category', $post)){
							get_template_part( 'templates/template_single_beau_et_bon' );
						} else if(has_term('diy-steps', 'category', $post)) {      
						   get_template_part( 'templates/template_single_diy_steps' );
						} else {
						   get_template_part( 'templates/template_single' );
						}
					?>

	            <?php endwhile; ?>
	        <?php endif; ?>
        </div>
	</main>
<?php get_footer(); ?>