$(document).ready(function () {

    //initialize swiper when document ready  
    var mySwiper = new Swiper ('.swiper-container', {
      // Optional parameters
      calculateHeight: true,
      direction: 'horizontal',
      loop: true,
      resistance: '100%',
      speed: 500,
	          // If we need pagination
  	  pagination: '.swiper-pagination',
  	    
  	    // Navigation arrows
  	  nextButton: '.swiper-button-next',
  	  prevButton: '.swiper-button-prev',
  	
  	  paginationType: 'fraction',

	  //hashnav: true,
	  onSlideChangeStart: function (mySwiper) {
	        // start Swiper slide
	      var urlslide = $(mySwiper.slides[mySwiper.activeIndex]).attr("data-url");
	      document.location.hash = urlslide;
          if(mySwiper.activeIndex != 1){
          console.log("CHANGE IMAGE SLIDER");
          lazyload();
          ga('send', 'pageview', { 'page': location.pathname + location.search + location.hash});
          ga('newTrackerFR.send', 'pageview', { 'page': location.pathname + location.search + location.hash});
          }
          
	    },
    })

    var mySwiperMobile = new Swiper ('.swiper-container-mobile', {
      // Optional parameters
      calculateHeight: true,
      direction: 'horizontal',
      loop: true,
      resistance: '100%',
      speed: 500,
              // If we need pagination
      pagination: '.swiper-pagination',
        
        // Navigation arrows
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
    
      paginationType: 'fraction',

    //hashnav: true,
    onSlideChangeStart: function (mySwiperMobile) {
          // start Swiper slide
          var urlslide = $(mySwiperMobile.slides[mySwiperMobile.activeIndex]).attr("data-url");
          document.location.hash = urlslide;
      },
    })

   /**** SWIPER HOME *****/
  //initialize swiper when document ready  
    var mySwiper = new Swiper ('.swiper-container-home', {
      // Optional parameters
      calculateHeight: true,
      direction: 'horizontal',
      loop: true,
      resistance: '100%',
      speed: 500,
	          // If we need pagination
  	  pagination: '.swiper-pagination',
  	    
  	    // Navigation arrows
  	  nextButton: '.swiper-button-next',
  	  prevButton: '.swiper-button-prev',
  	
  	  paginationType: 'fraction',
    })

    var mySwiperMobile = new Swiper ('.swiper-container-mobile-home', {
      // Optional parameters
      calculateHeight: true,
      direction: 'horizontal',
      loop: true,
      resistance: '100%',
      speed: 500,
              // If we need pagination
      pagination: '.swiper-pagination',
        
        // Navigation arrows
      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
    
      paginationType: 'fraction',

    })

    $('#swipeboxSlider .swipeboxExampleImg').click(function() {
    		console.log("CLICK IMG");
    		document.location.hash = $(this).attr("data-url");
    });


  });