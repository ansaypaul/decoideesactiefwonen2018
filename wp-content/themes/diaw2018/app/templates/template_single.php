<?php 
				global $post;
				$category = get_the_category( get_the_ID() );
				$tags =  wp_get_post_tags($post->ID);
				foreach ($tags as $tag) {
					$list_tags .=  $tag->name . ', ';
				}
			?>
			<article class="article" data-id = "<?php the_ID(); ?>" 
			data-cat = "<?php echo $category[0]->term_id; ?>" data-title="<?php the_title(); ?>" data-adposition="<?php echo mb_strtoupper(ad_position()['cat']); ?>" 
			data-adpositionclean = "<?php echo ad_position()['cat']; ?>" data-adpositionbis="<?php echo html_entity_decode(ad_position()['subcat']); ?>"
			data-author = "<?php the_author(); ?>" data-tag="<?php echo html_entity_decode(ad_position_ter()); ?>" data-link="<?php the_permalink(); ?>">
				<div class="main-content">
					<div class="header-content">
						<div class="post-cat _background">
							<?php 
								foreach ($category as $categorie) {
									if ($categorie->slug != 'slider'){
										echo '<a href="' . get_category_link($categorie->term_id). '" rel="bookmark">' .'#'. $categorie->name . '</a>';
									}
								}
							?>
						</div>
						<h1 class="post-title"><?php the_title(); ?></h1>
						<?php if(the_field('sous-titre')) { ?> <p class="post-subtitle"><?php the_field('sous-titre'); ?></p><?php } ?>
						<?php if(the_field('ondertitel')) { ?> <p class="post-subtitle"><?php the_field('ondertitel'); ?></p><?php } ?>
						<br/>
						<div class="pub-date">
							<?php _e( "Publié le", "html5blank" ); ?> <time class="post-date" datetime="<?php 'Y-m-d' ?>">
							<?php the_time('d.m.Y') ?></time>&nbsp;
						</div>
						<?php 
							$user_email = get_the_author_meta( 'user_email' );
							$user_title = get_the_author_meta( 'authortitle' ); 
							$user_descr = get_the_author_meta( 'description' );
							$user_instagram = get_the_author_meta( 'instagram' ); 
							$user_pinterest = get_the_author_meta( 'pinterest' ); 
							$fname = get_the_author_meta('first_name');
							$lname = get_the_author_meta('last_name');
							$ID = get_the_author_meta( 'ID' );

						?>
						<div class="post-author author vcard">
							<?php _e( "par", "html5blank" ); ?> 
							<a class="url fn n author-name _color" href="<?php echo esc_url( get_author_posts_url( $ID ) ); ?>"><?php the_author(); ?>
							</a>
						</div>
						<div class="post-info">
							<div class="share">
								<a href="http://www.facebook.com/sharer/sharer.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>" class="sprite sprite-fb" target="_blank"></a>
								<a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" class="sprite sprite-twitter" target="_blank"></a>
								<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php print(urlencode(the_post_thumbnail_url())); ?>&url=<?php print(urlencode(get_permalink())); ?>&is_video=false&description=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>" class="sprite sprite-pint" target="_blank"></a>
								<a href="mailto:?subject=<?php _e( "DECOIDEES.be", "html5blank" ); ?> | <?php print(urlencode(the_title())); ?>&amp;body=<?php print(urlencode(get_permalink())); ?>"class="sprite sprite-mail"></a>
							</div>
						</div>
					</div>
<!-- single.php -->
<?php if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){ 
	$slug_concours = 'concours'; 
} else {  
	$slug_concours = 'wedstrijd'; 
}?>
<?php if(get_the_category()[0]->slug != $slug_concours && get_the_category()[1]->slug != $slug_concours && get_the_category()[2]->slug != $slug_concours ) { ?>
	<div class="visuel">
		<?php $value = get_field( "copyright" ); ?>
		<?php the_post_thumbnail('d_single_n',array('',0,1,1)); ?>
		<span class="copyright">
	    	<span class="credit _background"> © </span>
		    <?php 
			if( $value ) { ?>
				<span class="photo-credit _background">
					<?php
				    	echo $value; 
				    ?>
			    </span>
			<?php } ?>
		</span>
	</div>
<?php } ?>
	<div class="content">
			<?php
				$content_replace = $post->post_content;
				$content_replace = str_replace('noopener', '', $content_replace);
				$content_replace = str_replace('<h1>', '<h2>', $content_replace);
				$content_replace = str_replace('</h1>', '</h2>', $content_replace);
			?>
			<?php echo apply_filters( 'the_content', $content_replace );  ?>
	</div>
</div>
<?php get_sidebar(); ?>
</article>
<section class="related-posts">
	<div class="main-related-title _color"><?php _e( "Vous aimerez aussi", "html5blank" ); ?></div>
	<?php 
	if(get_field('variable_js','option')['lang_identifiant'] == 'FR'){
		$category_publi = 2050;
	}else{
		$category_publi = 1274;
	}
	if(get_the_category()[0]->cat_ID != $category_publi) { 
	?>
	<?php 
	$related = new WP_Query( array( 'category__in' => wp_get_post_categories($post->ID), 'posts_per_page' => 3, 'post__not_in' => array($post->ID) )); 

	?>
	<?php  if ( $related->post_count >= 1)  { ?>
		<?php while ( $related->have_posts() ) : $related->the_post(); ?>
			<div class="row">
				<?php get_template_part( 'templates/template_related' ); ?>
				</div>
			<?php wp_reset_postdata(); ?>
		<?php endwhile; ?>
	<?php }
		} 
	?> 
	<?php 

	$query = new WP_Query( array ( 'category__in' => $category_publi , 'posts_per_page' => 3,  'order' => 'DESC')); 

	?>
</section>
<!-- Intégration OUTBRAIN -->
<section class="related-posts">
	<?php 
		global $wp;
		$current_url = home_url( add_query_arg( array(), $wp->request ) );
	?>			
	<div class="OUTBRAIN" data-src="<?php echo $current_url; ?>" data-widget-id="GS_4"></div> 
	<script type="text/javascript" async="async"src="//widgets.outbrain.com/outbrain.js"></script>
</section>
<!-- Intégration OUTBRAIN -->

<div id="billboard-<?php display_id_random()?>" class="ads ads-first TopLarge adsence-billboard" data-categoryAd="" data-formatMOB="MOB640x150" data-refadMOB="pebbleMOB640x150" data-format="TopLarge" data-refad="pebbleTopLarge" data-location="" data-position=""></div>