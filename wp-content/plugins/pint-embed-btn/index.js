(function() {
    tinymce.create("tinymce.plugins.pint_embed_plugin", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            //add new button    
            ed.addButton("pint", {
                title : "Pint Embed btn",
                cmd : "pint_command",
                image : "http://www.editionventures.be/images/pint.png"
            });

            //button functionality.
            ed.addCommand("pint_command", function() {
                var selected_text = ed.selection.getContent();
                var return_text = "<p class='pint-embed'><a data-pin-do='embedPin' data-pin-lang='fr' data-pin-width='medium' href='"+ selected_text +"'>Pint Embed Link</a><p>";
                ed.execCommand("mceInsertContent", 0, return_text);
            });

        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "Extra Buttons",
                author : "Elodie Buski",
                version : "1"
            };
        }
    });

    tinymce.PluginManager.add("pint_embed_plugin", tinymce.plugins.pint_embed_plugin);
})();