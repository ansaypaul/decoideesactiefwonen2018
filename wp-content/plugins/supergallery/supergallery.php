<?php
/**
 * Plugin Name: SuperGallery
 * Description: (-(-_-)-)*
 * Version: 1.0.0
 * Author: Ansay Paul
 * Author URI: mailto:ansaypaul@gmail.com
 */

// add copyright to caption function
//////////////////////////////////////////////////////////
// add custom field to image editor


function SuperGalleryCSS() {
  global $post;
  if(is_single()){
			wp_register_style( 'idangeroCSS', plugins_url( '/idangero/css/idangero.css', __FILE__ ) );
			wp_enqueue_style( 'idangeroCSS' );
		}
}
add_action( 'wp_enqueue_scripts', 'SuperGalleryCSS' );


function SuperGalleryJS(){
  global $post;
  //var_dump($post); 
	   if(is_single()){
		wp_register_script( 'justifiedGalleryJSinit',  plugins_url( '/justifiedGallery/jquery.justifiedGalleryinit.js', __FILE__ ), array( 'jquery' ));
		wp_enqueue_script( 'justifiedGalleryJSinit');
	}
}

//add_action( 'wp_head', 'SuperGalleryJS' );

function wp_get_attachment( $attachment_id ) {
  $attachment = get_post( $attachment_id );
  $args = array(
      'id' => $attachment->ID,
      'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
      'caption' => $attachment->post_excerpt,
      'description' => $attachment->post_content,
      'href' => get_permalink( $attachment->ID ),
      'src' => $attachment->guid,
      'title' => $attachment->post_title,
      'desc_rich'=> get_field('desc_rich',$attachment->ID),
      'shop_link'=> get_field('shop_link',$attachment->ID),
      'shop_price'=> get_field('shop_price',$attachment->ID),
      'slug'=> basename(get_permalink( $attachment->ID )) 
  );
  if($args['desc_rich'] == ''){$args['desc_rich'] = $attachment->post_excerpt; }
  return $args;
}

add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );

function my_post_gallery( $output, $attr) {
    global $post, $wp_locale, $idgallery;
    setup_postdata( $post );
    $idgallery++;
    if($attr['columns'] == null){ $attr['columns'] = 3; }
    $output = '';
	$ids = explode( ',', $attr['ids'] );
	//$numberpics = count($ids);
    //echo  $numberpics;
    //$output .= $attr['ds_title'].'<br/><br/>';
   /*if($attr['ds_select'] == 'swiper'){
			$homepic = wp_get_attachment($ids[0]);
			$numberpics = count($ids);
			$output .= '<div class="swiper-main">'; 
			$output .= '<div class="swiper-container swiper-container-shop">'; 
			$output .= '<div class="swiper-wrapper">'; 
			$legend = '';
			$i = 1;
				foreach($ids as $id) 
				{
					$pic = wp_get_attachment($id);
					//var_dump($pic);
					$output .= '<div class="swiper-slide" data-url="'.$pic['slug'].'">';
					$output .= wp_get_attachment_image($pic['id'],'d_swiper');
					$output .= '<div class="swiper-compteur">'.$i.'/'.$numberpics.'</div>';
					$output .= '<div class="legend">'.$pic['desc_rich'].'</div>';
					if($pic['shop_link'] != null){
					$output .= '<span class="shop-button"><a target="_blank" href="'.$pic['shop_link'].'">SHOP IT</a></span>';
					}
					$output .= '</div>';
					$i++;
				}
			$output .= '</div><div class="swiper-button-prev"></div><div class="swiper-button-next"></div></div></div>';
		/*}else{*/

			$homepic = wp_get_attachment($ids[0]);
			$numberpics = count($ids);
			$output .= '<div class="gallery-main">'; 
			$output .= '<div class="gallery-container">'; 
			$output .= '<div class="gallery-wrapper">'; 
			$legend = '';
			$i = 1;
				foreach($ids as $id) 
				{
					$pic = wp_get_attachment($id);
					//var_dump($pic);
					$output .= '<div class="gallery-slide" data-url="'.$pic['slug'].'">';
          $output .= '<div class="gallery-compteur">'.$i.'/'.$numberpics.'</div>';
					$output .= wp_get_attachment_image($pic['id'],'d_swiper');
					$output .= '<div class="legend">'.$pic['desc_rich'].'</div>';
					if($pic['shop_link'] != null){
					$output .= '<span class="shop-button"><a target="_blank" href="'.$pic['shop_link'].'">SHOP IT</a></span>';
					}
					$output .= '</div>';
					if($i == 3){ $output .= '<div id="imu-'._display_id_random().'" class="ads halfpage" data-categoryAd="" data-formatMOB="MMR" data-refadMOB="pebbleMMR" data-format="" data-refad="" data-location="others" data-position="2"></div>';
					}
					$i++;
				}
				$output .= '</div></div></div>';

		/*}*/
	
    return $output;

}


add_action('print_media_templates', function(){
?>
<script type="text/html" id="tmpl-custom-gallery-setting">
    <h3 style="z-index: -1;">________________________________________________________________________________________________________________________________________________________</h3>
    <h3>SuperGallery Options</h3>
     <!--<label class="setting">
        <input type="text" value="" data-setting="ds_title" style="float:left;">
    </label>-->

     <!--<label class="setting">
        <span>Photo d'ouverture : (url media http)</span>
        <input type="text" value="" data-setting="ds_text" style="float:left;">
    </label>-->
	<!--
    <label class="setting">
        <span><?php _e('Textarea'); ?></span>
        <textarea value="" data-setting="ds_textarea" style="float:left;"></textarea>
    </label>

    <label class="setting">
        <span><?php _e('Number'); ?></span>
        <input type="number" value="" data-setting="ds_number" style="float:left;" min="1" max="9">
    </label>-->
    <label class="setting">
      <span>Une seul image : </span>
      <select data-setting="ds_firstpic">
        <option value="Oui">Oui</option>
        <option value="Non">Non</option>
      </select>
    </label>

    <label class="setting">
      <span>Type : </span>
      <select data-setting="ds_select">
      	<option value=""></option>
        <option value="swiper">Swiper</option>
     	<option value="Classique">Classique</option>
        <option value="Mosaique">Mosaïque Shop</option>
       <!-- <option value="Slider">Slider</option> -->
        <!--<option value="Masonry">Masonry</option>-->
        <!--<option value="Masonry/fancybox">Masonry/Fancybox</option>-->
        <!-- <option value="fancybox">fancybox</option> -->
        <option value="Shop">Shop</option>
      </select>
    </label>

    <!--
    <label class="setting">
      <span>Orientation : </span>
      <select data-setting="ds_orientation">
        <option value="Horizontal">Horizontal</option>
        <option value="Vertical">Vertical</option>
      </select>
    </label>
    -->

    <label class="Légende">
      <span>Légende : </span>
      <select data-setting="ds_legende">
        <option value="Oui">Oui</option>
        <option value="Non">Non</option>
      </select>
    </label>


   <!-- <label class="setting">
        <span><?php _e('Bool'); ?></span>
        <input type="checkbox" data-setting="ds_bool">
    </label>-->

</script>
<script>

jQuery(document).ready(function()
    {
        _.extend(wp.media.gallery.defaults, {
        ds_title: 'no text',
        ds_text: 'no text',
        ds_textarea: 'no more text',
        ds_number: "3",
        ds_select: '',
        ds_orientation: '',
        ds_legende: '',
        ds_firstpic: 'Non',
        ds_bool: false,
        ds_text1: 'dummdideldei'
        });

        wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('custom-gallery-setting')(view);
        }
        });

    });

</script>
<?php

});

// let's start by enqueuing our styles correctly
function wptutsplus_admin_styles() {
    wp_register_style( 'wptuts_admin_stylesheet', plugins_url( '/css/admin.css', __FILE__ ) );
    wp_enqueue_style( 'wptuts_admin_stylesheet' );
}

add_action( 'admin_enqueue_scripts', 'wptutsplus_admin_styles' );

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_SuperGallery',
	'title' => 'SuperGallery',
	'fields' => array (
		array (
			'key' => 'desc_rich',
			'label' => 'Description',
			'name' => 'description',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 0,
			'delay' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'attachment',
				'operator' => '==',
				'value' => 'all',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>