// external js: masonry.pkgd.js, imagesloaded.pkgd.js

// init Isotope
var grid = document.querySelector('.grid');
var msnry;

//imagesLoaded( grid, function() {
  // init Isotope after all images have loaded
  /*msnry = new Masonry( grid, {
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
     });


	function onLayout() {
	  console.log('layout done');
	}
	// bind event listener
	msnry.on( 'layoutComplete', onLayout );
	// un-bind event listener
	//msnry.off( 'layoutComplete', onLayout );
	// bind event listener to be triggered just once
	//msnry.once( 'layoutComplete', function() {
	 // console.log('layout done, just this one time');
	//});*/

	var $grid = jQuery('.grid').masonry({
		horizontalOrder: true
	  // options...
	});
	// layout Masonry after each image loads
	$grid.imagesLoaded().progress( function() {
	  $grid.masonry('layout');
	});
/*
});*/
