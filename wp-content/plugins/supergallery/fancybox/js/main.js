jQuery(document).ready(function(){
	jQuery( '[data-fancybox]' ).fancybox({
		touch : true,
		baseTpl	: '<div class="fancybox-container" role="dialog" tabindex="-1">' +
					'<div class="fancybox-bg"></div>' +
					'<div class="fancybox-controls"><img class="logo-mcmobile" src="http://marieclaire.be/fr/wp-content/themes/marieclaire2017/site/assets/img/mc-logo-wht.svg" title="Marie Claire Belgique" alt="Marie Claire Belgique logo">' +
						'<div class="fancybox-infobar">' +
							'<div class="fancybox-infobar__body">' +
								'<span class="js-fancybox-index"></span>&nbsp;/&nbsp;<span class="js-fancybox-count"></span>' +
							'</div>' +
						'</div>' +
						'<div class="fancybox-buttons">' +
							'<button data-fancybox-close class="fancybox-button fancybox-button--close" title="Close (Esc)"></button>' +
						'</div>' +
					'</div>' +
					'<div class="fancybox-slider-wrap">' +
					'<button data-fancybox-previous class="fancybox-button fancybox-button--left" title="Previous"></button>' +
					'<button data-fancybox-next class="fancybox-button fancybox-button--right" title="Next"></button>' +

						'<div class="fancybox-slider"></div>' +
					'</div>' +
					'<div class="fancybox-caption-wrap"><div class="caption-top"><img class="logo-mcdesktop" src="http://marieclaire.be/fr/wp-content/themes/marieclaire2017/site/assets/img/mc-logo-wht.svg" title="Marie Claire Belgique" alt="Marie Claire Belgique logo"><div class="fancybox-caption"></div></div>'+
					'<div class="fancybox-AD caption-bottom"><div id="affiche-box-shop" class="over-halfpage"><div id="affiche-shop" class="affiche"><div id="article-shop-1" class="lazyload" formatMOB="MOB640x150" refadMOB="pebbleMOB640x150" format="MiddleLarge" refad="pebbleMiddleLarge" location="others" position="" category="gallery"></div></div></div></div>'+
					'</div>' +
				'</div>',

			image : {
				protect: true
			},
		beforeMove 	: function( instance, slide ) {
			console.info('Fancybox switch' );
			ga('send', 'pageview', { 'page': location.pathname + location.search + location.hash});
          	ga('newTrackerFR.send', 'pageview', { 'page': location.pathname + location.search + location.hash});
			lazyload();
		},

		  caption : function( instance, item ) {
		    var caption, link, divad, shop_link;

		    if ( item.type === 'image' ) {
		      //shoplink = '<span class="shop-button"><a target="_blank" href="'.$(this).data('shoplink').'">SHOP IT</a></span>';	
		      	shop_link = jQuery(this).data('shoplink');
		     	if(typeof(shop_link) != 'undefined'){
		     	 	caption = jQuery(this).data('caption') + '<span class="shop-button"><a target="_blank" href="'+shop_link+'">SHOP IT</a></span>';
		  		}else{
					caption = jQuery(this).data('caption');
			  	}
		      //caption = '<span class="shop-button"><a target="_blank" href="http://www.google.fr">SHOP IT</a></span>';
		      //shoplink = $(this).data('shoplink');
		      divad   = '' ;
		      return caption + '<br />'+ divad;
		    }

		  }
		});
});