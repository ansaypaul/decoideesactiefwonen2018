<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="<?php bloginfo("description"); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php bloginfo("name"); ?></title>
        <!-- CSS -->
        <link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/assets/css/style.min.css">
		<link rel="stylesheet" href="<?php bloginfo("template_directory"); ?>/assets/css/plugins/swiper-2.7.6/idangerous.swiper.css">
        <link rel="author" href="Elodie Buski">
		<!-- FAVICON -->
        <link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php bloginfo("template_directory"); ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
		<?php wp_head(); ?> 
    </head>
    <body <?php 
			if ( is_home() OR is_page('index') ) {
				echo "id='home'";
			} else {
			  	echo "id='single'";
			};
		?>> 

   		<div class="search-wrapper">
   			<div class="search-closebtn"><span></span></div>
   			<form role="search" method="get" id="search-form" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
   				<input placeholder="Rechercher..." type="text" value="<?php echo get_search_query(); ?>">
				<input value="Rechercher" type="submit" value="<?php echo esc_attr_x( 'recherche', 'submit button' ); ?>">
			</form>
   		</div>
   		<div class="backtotop"></div>
   		<header id="header" class="fixed">
				<?php if( is_home() OR is_page('index') ): ?>
				    <div class="swiper-container">
				    	<div class="container">
					    	<a class="link-logo" href="<?php bloginfo('url'); ?>">
			   					<img class="logo" src="<?php bloginfo("template_directory"); ?>/assets/img/DecoIdees_logo_b.svg" alt="Déco idées logos">
			   					<h1>donne vie à vos envies</h1>
			   				</a>
		   				</div>
				        <div class="swiper-wrapper">
				        	<?php global $excluded_ID; ?>
				        	<!-- //Offline News ID=3 // Inspirations ID=4 //Astuces ID=5 // Bonnes adresses ID=6 -->
				        	<!-- //Online News ID=7 // Inspirations ID=5 //Astuces ID=2 // Bonnes adresses ID=3 -->
				        	<!-- //Actu ID=32 // PiècesparPièces ID=25 //Décopratique ID=24 // Bonnes adresses ID=26 -->
							<?php $categories=get_categories('include=24,25,26,32&orderby=name'); ?>
							  <?php foreach($categories as $category) { ?>
							  	<!-- Offline Slider ID=14 -->
							  	<!-- Online Slider ID=11 -->
							  	<!-- Slider ID=2033 -->
							    <?php $the_query = new WP_Query( array( 'category__and' => array( $category->term_id, 2033 ), 'posts_per_page' => 1 ) ); ?>
							        <?php if ( $the_query->have_posts() ) : ?>
							        	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							        		<?php $excluded_ID[] = get_the_ID(); ?>
									  		<div class="swiper-slide">
												<div class="container">
													<article>
											    		<span class="category"><a href="<?php echo get_category_link($category->term_id ) ?>"><?php echo $category->name; ?></a></span>
														<h2>
															<a href="<?php the_permalink(); ?>" class="link-article"><?php the_title(); ?></a>
														</h2>
														<?php if ( ! has_excerpt() ) {
														      echo '';
														} else { 
														      the_excerpt();
														} ?>
											    	</article>
												</div>
												<div class="cover">
													<?php the_post_thumbnail('slider'); ?>
												</div>
											</div>
										<?php endwhile; ?>
								<?php endif; ?>
							<?php } ?> 
				        </div>
					</div>
		        <?php endif; ?>
        	<div class="menu-wrapper <?php 
				if ( !is_home() ) {
				  	echo 'fixed';
				};
			?>"> 
	        	<div class="menu <?php 
				if ( !is_home() ) {
				  	echo 'fixed';
				};
			?>"> 
					<div class="container">
						<nav>
		        		<ul>
		        			<li class="logo">
		        				<a href="http://www.decoidees.be/">
		        					<svg class="di_logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.74 81.09"><title>Déco Idées logo</title><rect x="320.82" y="15.09" width="3.06" height="45.2"/><path d="M366.52,38c0,5,0,12.49-3.45,16-2.35,2.41-5.6,3.58-9.93,3.58H341.92V17.78h11.21c4.33,0,7.58,1.17,9.93,3.58,3.48,3.55,3.47,9.76,3.45,14.76V38Zm-1.3-18.79c-2.68-2.69-6.58-4.11-11.26-4.11h-15.1v45.2H354c4.68,0,8.57-1.42,11.26-4.1,4.38-4.38,4.37-12.49,4.36-18.43V36.34c0-6,0-12.77-4.36-17.14"/><polygon points="406.98 17.77 406.98 15.09 379.71 15.09 379.71 60.29 406.98 60.29 406.98 57.61 382.78 57.61 382.78 38.84 403.39 38.84 403.39 36.16 382.78 36.16 382.78 17.77 406.98 17.77"/><polygon points="443.98 17.77 443.98 15.09 416.72 15.09 416.72 60.29 443.98 60.29 443.98 57.61 419.78 57.61 419.78 38.84 440.4 38.84 440.4 36.16 419.78 36.16 419.78 17.77 443.98 17.77"/><path d="M475.08,39.57c-2-1.63-3.91-2.35-8-3l-4.93-.77A14.67,14.67,0,0,1,455.23,33c-1.64-1.38-2.44-3.38-2.44-6.1,0-5.93,4.12-9.61,10.76-9.61a15,15,0,0,1,11.07,4.15l2-2a17.85,17.85,0,0,0-12.84-4.75c-8.61,0-14,4.71-14,12.29a10.23,10.23,0,0,0,3.52,8.24,15.74,15.74,0,0,0,8,3.28l5.32,0.83c3.75,0.58,5,1,6.69,2.48a8.43,8.43,0,0,1,2.57,6.55c0,5.93-4.57,9.61-11.91,9.61-5.65,0-8.94-1.34-12.94-5.3l-2.15,2.16c4.24,4.19,8.44,5.82,14.89,5.82,9.36,0,15.17-4.76,15.17-12.42A11,11,0,0,0,475.08,39.57Z"/><polygon points="101.82 42.97 133.9 42.99 133.88 38.81 101.82 38.81 101.82 6.85 146.09 6.84 146.04 2.11 89.22 2.11 89.22 81.05 146.17 81.05 146.17 76.23 101.81 76.23 101.82 42.97" /><path d="M279.5,76.83a41.86,41.86,0,0,0,12.57-8.16c7.27-6.88,10.81-16.06,10.81-28.08,0-12.77-3.79-22.07-11.92-29.26C282.36,3.82,270.74,0,256.41,0c-14.89,0-27.22,4.29-35.66,12.41-7.37,7-11,16.19-11,28.19,0,12.46,3.65,21.55,11.5,28.61a40.46,40.46,0,0,0,10.93,7.16l-35.78,0c-12.11-.66-20.33-1.67-29.09-10.19-6.34-6.07-10-15.39-10-25.57,0-11.27,4.62-20.51,13.35-26.72,9.09-6.48,17.28-7,29.68-7H211l0-4.8-20.83,0c-14.81,0-24.84,2.89-33.52,9.66-8.19,6.29-12.88,16.5-12.88,28,0,12,4.58,22.65,12.91,29.92C167.7,79.27,177.53,81.06,191.84,81l285.85,0,0-4.24H279.5Zm-49.41-12C223.95,56.35,223,46,223,40.6c0-17.66,8.77-36.53,33.38-36.53,11.54,0,20.36,4.11,26.19,12.21,6.13,8.51,7,18.89,7,24.32s-0.92,15.74-7,24.23C276.76,72.92,267.95,77,256.41,77S236,72.92,230.09,64.83Z"/><path d="M68.67,11.77C60,5,50,2.11,35.15,2.11q-1.4,0-2.84,0H0v76.1H0v2.84H33.56c14.31,0,24.15-1.81,35.08-11.38C77,62.43,81.55,51.81,81.55,39.79,81.55,28.28,76.86,18.07,68.67,11.77ZM58,66.13c-8.76,8.52-17,10.13-29.09,10.13l-16.27,0v-0.1h0V6.88H25c12.4,0,20.59.47,29.68,7C63.4,20,68,29.29,68,40.56,68,50.74,64.39,60.06,58,66.13Z"/></svg>
		        				</a>
		        			</li>
		        			<li class="burger"><span></span></li>
		        			<li class="responsive">

		        				<?php wp_nav_menu( 
	        						array( 
	        							'theme_location' => 'main-menu', 
	        							'container' => '',
	        							'items_wrap'      => '<ul class="responsive-ul">%3$s',
	        							'link_before' => '<span>',
                    					'link_after' => '</span><span></span>'
	        						) 
	        					); ?>
				        			<li class="lang" class="hover-effect">
				        				<ul class="lang-ul">
				        					<li><a href="#" class="hover-effect"><span>FR</span><span>FR</span></a></li>
				        					<li><a href="#" class="hover-effect"><span>NL</span><span>NL</span></a></li>
				        				</ul>
				        			</li>
				        			<li class="search" class="hover-effect">
				        				<img src="<?php bloginfo("template_directory"); ?>/assets/img/search.png" alt="Search">
				        				<img src="<?php bloginfo("template_directory"); ?>/assets/img/search-g.png" alt="Search">
				        			</li>
				        			<li class="social" class="hover-effect">
				        				<a href="https://www.facebook.com/DECO-id%C3%A9es-173570092719065/" target="_blank" alt="Facebook">
				        					<img src="<?php bloginfo("template_directory"); ?>/assets/img/facebook.png">
				        					<img src="<?php bloginfo("template_directory"); ?>/assets/img/facebook-g.png">
				        				</a>
				        				<a href="https://www.instagram.com/decoidees_actiefwonen/" target="_blank" alt="Instagram">
				        					<img src="<?php bloginfo("template_directory"); ?>/assets/img/instagram.png">
				        					<img src="<?php bloginfo("template_directory"); ?>/assets/img/instagram-g.png">
				        				</a>
				        			</li>
		        				</ul>
		        			</li>
		        			<!-- COVER PIC CSS -->
		        			<li class="store"><a href="http://www.viapress.be/3188/abonnement-magazine-deco-idees.html" target="_blank">le n°255<br>est en librairie</a></li>
		        		</ul>
		        		</nav>
		        	</div>
	        	</div>
        	</div>
		</header>
		<!-- Cookie box -->
	<section class="main-wrapper">